@extends('auth.master')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 order-md-2">
                <img src="https://preview.colorlib.com/theme/bootstrap/login-form-08/images/undraw_file_sync_ot38.svg"
                    alt="Image" class="img-fluid">
            </div>
            <div class="col-md-6 contents">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="mb-4">
                            <h3>Đăng nhập</h3>
                            {{-- <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur
                                adipisicing.</p> --}}
                        </div>
                        <form action="{{ route('voyager.login') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group first">
                                <label>Email</label>
                                <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                            </div>
                            <div class="form-group last mb-4">
                                <label for="password">Mật khẩu</label>
                                <input type="password" name="password" placeholder="{{ __('voyager::generic.password') }}" class="form-control" required>
                            </div>
                            <div class="d-flex mb-5 align-items-center">
                                <label class="control control--checkbox mb-0">
                                    <span class="caption">Ghi nhớ</span>
                                    <input type="checkbox" checked="checked">
                                    <div class="control__indicator"></div>
                                </label>
                                <span class="ml-auto"><a href="#" class="forgot-pass">Quên mật khẩu</a></span>
                            </div>
                            <input type="submit" value="Đăng nhập" class="btn text-white btn-block btn-primary">
                            {{-- <span class="d-block text-left my-4 text-muted"> or sign in with</span>
                            <div class="social-login">
                                <a href="#" class="facebook">
                                    <span class="icon-facebook mr-3">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </span>
                                </a>
                                <a href="#" class="twitter">
                                    <span class="icon-twitter mr-3">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </span>
                                </a>
                                <a href="#" class="google">
                                    <span class="icon-google mr-3">
                                        <i class="fa fa-google" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </div> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection