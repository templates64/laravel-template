@php
    $currentRouteName = Route::currentRouteName();
@endphp
<div id="preloder" style="display: none;">
    <div class="loader" style="display: none;"></div>
</div>

<div class="humberger__menu__overlay"></div>
<div class="humberger__menu__wrapper">
    <div class="humberger__menu__logo">
        <a href="#"><img src="https://preview.colorlib.com/theme/ogani/img/logo.png" alt=""></a>
    </div>
    <div class="humberger__menu__cart">
        <ul>
            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
        </ul>
        <div class="header__cart__price">item: <span>$150.00</span></div>
    </div>
    <div class="humberger__menu__widget">
        <div class="header__top__right__language">
            <img src="https://preview.colorlib.com/theme/ogani/img/language.png" alt="">
            <div>English</div>
            <span class="arrow_carrot-down"></span>
            <ul>
                <li><a href="#">Spanis</a></li>
                <li><a href="#">English</a></li>
            </ul>
        </div>
        <div class="header__top__right__auth">
            <a href="#"><i class="fa fa-user"></i> Login</a>
        </div>
    </div>
    <nav class="humberger__menu__nav mobile-menu">
        <ul>
            <li class="{{$currentRouteName == 'home' ? 'active': ''}}"><a href="{{route('home')}}">Home</a></li>
            <li><a href="{{route('products.index')}}">Shop</a></li>
            <li><a href="#">Pages</a>
                <ul class="header__menu__dropdown">
                    <li><a href="{{route('products.detail', ['id' => 1, 'slug' => 'product-example'])}}">Shop Details</a></li>
                    <li><a href="{{route('products.cart')}}">Shoping Cart</a></li>
                    <li><a href="{{route('products.checkout')}}">Check Out</a></li>
                    <li><a href="{{route('posts.detail', ['id' => 1, 'slug' => 'post-samplate'])}}">Blog Details</a></li>
                </ul>
            </li>
            <li><a href="{{route('posts.index')}}">Blog</a></li>
            <li><a href="{{route('contact')}}">Contact</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap">
        <div class="slicknav_menu"><a href="#" aria-haspopup="true" role="button" tabindex="0"
                class="slicknav_btn slicknav_collapsed"><span class="slicknav_menutxt">MENU</span><span
                    class="slicknav_icon"><span class="slicknav_icon-bar"></span><span
                        class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></a>
            <nav class="slicknav_nav slicknav_hidden" style="display: none;" aria-hidden="true" role="menu">
                <ul>
                    <li class="{{$currentRouteName == 'home' ? 'active': ''}}"><a href="{{route('home')}}" role="menuitem">Home</a></li>
                    <li><a href="{{route('products.index')}}" role="menuitem">Shop</a></li>
                    <li class="slicknav_collapsed slicknav_parent"><a href="#" role="menuitem" aria-haspopup="true"
                            tabindex="-1" class="slicknav_item slicknav_row"><a href="#">Pages</a>
                            <span class="slicknav_arrow">►</span></a>
                        <ul class="header__menu__dropdown slicknav_hidden" role="menu" style="display: none;"
                            aria-hidden="true">
                            <li><a href="{{route('products.detail', ['id' => 1, 'slug' => 'product-example'])}}" role="menuitem" tabindex="-1">Shop Details</a></li>
                            <li><a href="{{route('products.cart')}}" role="menuitem" tabindex="-1">Shoping Cart</a></li>
                            <li><a href="{{route('products.checkout')}}" role="menuitem" tabindex="-1">Check Out</a></li>
                            <li><a href="{{route('posts.detail', ['id' => 1, 'slug' => 'post-samplate'])}}" role="menuitem" tabindex="-1">Blog Details</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('posts.index')}}" role="menuitem">Blog</a></li>
                    <li><a href="{{route('contact')}}" role="menuitem">Contact</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="header__top__right__social">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
        <a href="#"><i class="fa fa-pinterest-p"></i></a>
    </div>
    <div class="humberger__menu__contact">
        <ul>
            <li><i class="fa fa-envelope"></i> admin@ogani.com</li>
            <li>Miễn khí vận chuyển cho đơn hàng trên 2200k</li>
        </ul>
    </div>
</div>


<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                            <li><i class="fa fa-envelope"></i> admin@ogani.com</li>
                            <li>Miễn khí vận chuyển cho đơn hàng trên 2200k</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                        {{-- <div class="header__top__right__language">
                            <img src="https://preview.colorlib.com/theme/ogani/img/language.png" alt="">
                            <div>English</div>
                            <span class="arrow_carrot-down"></span>
                            <ul>
                                <li><a href="#">Spanis</a></li>
                                <li><a href="#">English</a></li>
                            </ul>
                        </div> --}}
                        @if (Auth::check())
                        <div class="header__top__right__auth">
                            <div>
                                <img src="{{env('MIX_S3_URL') . Auth::user()->avatar}}" alt="">
                                Xin chào! {{Auth::user()->name}}
                            </div>
                        </div>
                        @else 
                        <div class="header__top__right__auth">
                            <a href="{{route('user.login')}}">
                                <i class="fa fa-user"></i> Đăng nhập
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="{{route('home')}}"><img src="https://preview.colorlib.com/theme/ogani/img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="header__menu">
                    <ul>
                        <li class="{{$currentRouteName == 'home' ? 'active': ''}}"><a href="{{route('home')}}">Home</a></li>
                        <li><a href="{{route('products.index')}}">Shop</a></li>
                        <li><a href="#">Pages</a>
                            <ul class="header__menu__dropdown">
                                <li><a href="{{route('products.detail', ['id' => 1, 'slug' => 'product-example'])}}">Shop Details</a></li>
                                <li><a href="{{route('products.cart')}}">Shoping Cart</a></li>
                                <li><a href="{{route('products.checkout')}}">Check Out</a></li>
                                <li><a href="{{route('posts.detail', ['id' => 1, 'slug' => 'post-samplate'])}}">Blog Details</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('posts.index')}}">Blog</a></li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header__cart">
                    <ul>
                        <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                        <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
                    </ul>
                    <div class="header__cart__price">item: <span>$150.00</span></div>
                </div>
            </div>
        </div>
        <div class="humberger__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>


<section class="hero {{$currentRouteName != 'home' ? 'hero-normal' : ''}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>All departments</span>
                    </div>
                    <ul>
                        <li><a href="#">Fresh Meat</a></li>
                        <li><a href="#">Vegetables</a></li>
                        <li><a href="#">Fruit &amp; Nut Gifts</a></li>
                        <li><a href="#">Fresh Berries</a></li>
                        <li><a href="#">Ocean Foods</a></li>
                        <li><a href="#">Butter &amp; Eggs</a></li>
                        <li><a href="#">Fastfood</a></li>
                        <li><a href="#">Fresh Onion</a></li>
                        <li><a href="#">Papayaya &amp; Crisps</a></li>
                        <li><a href="#">Oatmeal</a></li>
                        <li><a href="#">Fresh Bananas</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="hero__search">
                    <div class="hero__search__form">
                        <form action="#">
                            <div class="hero__search__categories">
                                All Categories
                                <span class="arrow_carrot-down"></span>
                            </div>
                            <input type="text" placeholder="What do yo u need?">
                            <button type="submit" class="site-btn">SEARCH</button>
                        </form>
                    </div>
                    <div class="hero__search__phone">
                        <div class="hero__search__phone__icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="hero__search__phone__text">
                            <h5>+65 11.188.888</h5>
                            <span>support 24/7 time</span>
                        </div>
                    </div>
                </div>
                @if ($currentRouteName == 'home')
                <div class="hero__item set-bg" data-setbg="https://preview.colorlib.com/theme/ogani/img/hero/banner.jpg"
                    style="background-image: url(&quot;https://preview.colorlib.com/theme/ogani/img/hero/banner.jpg&quot;);">
                    <div class="hero__text">
                        <span>FRUIT FRESH</span>
                        <h2>Vegetable <br>100% Organic</h2>
                        <p>Free Pickup and Delivery Available</p>
                        <a href="#" class="primary-btn">SHOP NOW</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
