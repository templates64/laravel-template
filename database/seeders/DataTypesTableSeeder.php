<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\DataType;

class DataTypesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'users');
        if ($dataType->exists) {
            $dataType->fill([
                'controller'            => 'App\\Http\\Controllers\\Admin\\UserController',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'products');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'products',
                'display_name_singular' => __('seeders.data_types.product.singular'),
                'display_name_plural'   => __('seeders.data_types.product.plural'),
                'icon'                  => 'sunio-shop',
                'model_name'            => 'App\\Models\\Product',
                'policy_name'           => 'App\\Policies\\ProductPolicy',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'user-metas');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'user_metas',
                'display_name_singular' => __('seeders.data_types.user_meta.singular'),
                'display_name_plural'   => __('seeders.data_types.user_meta.plural'),
                'icon'                  => 'sunio-list-add',
                'model_name'            => 'App\\Models\\UserMeta',
                'policy_name'           => 'App\\Policies\\UserMetaPolicy',
                'controller'            => 'App\\Http\\Controllers\\Admin\\UserMetaController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
