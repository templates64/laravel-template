<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\Category;
use SIO\Sunio\Models\DataRow;
use SIO\Sunio\Models\DataType;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        //Data Rows
        $categoryDataType = DataType::where('slug', 'categories')->firstOrFail();

        $dataRow = $this->dataRow($categoryDataType, 'category_type');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => __('seeders.data_rows.category_type'),
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 4,
                'details'      => [
                    'default' => 'post',
                    'options' => [
                        'post' => 'post',
                        'product'     => 'product',
                    ],
                ],
            ])->save();
        }

        //Content
        $category = Category::firstOrNew([
            'slug' => 'category-3',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name' => 'Category 3',
                'category_type' => 'product',
            ])->save();
        }

        $category = Category::firstOrNew([
            'slug' => 'category-4',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name' => 'Category 4',
                'category_type' => 'product',
            ])->save();
        }
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
