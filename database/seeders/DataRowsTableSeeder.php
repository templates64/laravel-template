<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\DataRow;
use SIO\Sunio\Models\DataType;

class DataRowsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $postDataType = DataType::where('slug', 'posts')->firstOrFail();
        $productDataType = DataType::where('slug', 'products')->firstOrFail();
        $userMetaDataType = DataType::where('slug', 'user-metas')->firstOrFail();

        $dataRow = $this->dataRow($postDataType, 'post_belongstomany_category_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'categories',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'scope'       => 'post',
                    'model'       => 'App\\Models\\Category',
                    'table'       => 'categories',
                    'type'        => 'belongsToMany',
                    'column'      => 'id',
                    'key'         => 'id',
                    'label'       => 'name',
                    'pivot_table' => 'post_categories',
                    'pivot'       => '1',
                    'taggable'    => null,
                ],
                'order'        => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($productDataType, 'product_belongstomany_category_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'categories',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'scope'       => 'product',
                    'model'       => 'App\\Models\\Category',
                    'table'       => 'categories',
                    'type'        => 'belongsToMany',
                    'column'      => 'id',
                    'key'         => 'id',
                    'label'       => 'name',
                    'pivot_table' => 'product_categories',
                    'pivot'       => '1',
                    'taggable'    => null,
                ],
                'order'        => 16,
            ])->save();
        }
        
        $dataRow = $this->dataRow($userMetaDataType, 'user_meta_hasone_user_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'User',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'model'       => 'App\\Models\\User',
                    'table'       => 'users',
                    'type'        => 'belongsTo',
                    'column'      => 'user_id',
                    'key'         => 'id',
                    'label'       => 'name',
                    'pivot_table' => 'categories',
                    'pivot'       => '0',
                    'taggable'    => null,
                ],
                'order'        => 16,
            ])->save();
        }

    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
