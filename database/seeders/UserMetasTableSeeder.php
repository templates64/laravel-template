<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\DataRow;
use SIO\Sunio\Models\DataType;
use SIO\Sunio\Models\Menu;
use SIO\Sunio\Models\MenuItem;
use App\Models\User;
use App\Models\UserMeta;
use SIO\Sunio\Facades\Sunio;
class UserMetasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        ////Data Rows
        $userMetaDataType = DataType::where('slug', 'user-metas')->firstOrFail();
        $dataRow = $this->dataRow($userMetaDataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => __('seeders.data_rows.id'),
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($userMetaDataType, 'user_id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => __('seeders.data_rows.user'),
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 2,
            ])->save();
        }
        
        $dataRow = $this->dataRow($userMetaDataType, 'meta_key');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => __('seeders.data_rows.meta_key'),
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($userMetaDataType, 'meta_value');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => __('seeders.data_rows.meta_value'),
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 3,
            ])->save();
        }
        
        // $options = [];
        // foreach (Sunio::formFields() as $key => $value) {
        //     $options[$key] = $value->getCodename();
        // }
        // $dataRow = $this->dataRow($userMetaDataType, 'input_type');
        // if (!$dataRow->exists) {
        //     $dataRow->fill([
        //         'type'         => 'select_dropdown',
        //         'display_name' => __('seeders.data_rows.input_type'),
        //         'required'     => 1,
        //         'browse'       => 0,
        //         'read'         => 1,
        //         'edit'         => 1,
        //         'add'          => 1,
        //         'delete'       => 1,
        //         'details'      => [
        //             'default' => 'text',
        //             'options' => $options,
        //         ],
        //         'order'        => 4,
        //     ])->save();
        // }


        $dataRow = $this->dataRow($userMetaDataType, 'created_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => __('sunio::seeders.data_rows.created_at'),
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 12,
            ])->save();
        }

        $dataRow = $this->dataRow($userMetaDataType, 'updated_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => __('sunio::seeders.data_rows.updated_at'),
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 13,
            ])->save();
        }


        //Menu Item
        $menu = Menu::where('name', 'admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
                    'menu_id' => $menu->id,
                    'title'   => __('seeders.menu_items.user_meta'),
                    'url'     => '',
                    'route'   => 'sunio.user-metas.index',
                ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                        'target'     => '_self',
                        'icon_class' => 'sunio-list-add',
                        'color'      => null,
                        'parent_id'  => null,
                        'order'      => 6,
                    ])->save();
        }

        //Content
        $users = User::all();
        foreach ($users as $user) {
            $meta_phone = $this->findUserMeta($user->id, 'phone');
            if (!$meta_phone->exists) {
                $meta_phone->fill([
                    'user_id' => $user->id,
                    'meta_key' => 'phone',
                    'meta_value' => '0973074528',
                    // 'input_type' => 'number',
                ])->save();
            }
            
            $meta_address = $this->findUserMeta($user->id, 'address');
            if (!$meta_address->exists) {
                $meta_address->fill([
                    'user_id' => $user->id,
                    'meta_key' => 'address',
                    'meta_value' => '27 ngõ 20 Nguyễn Chánh - Trung Hòa - Cầu Giấy - HN',
                    // 'input_type' => 'text',
                ])->save();
            }

        }
        
    }

    /**
     * [user meta description].
     *
     * @param [type] $slug [description]
     *
     * @return [type] [description]
     */
    protected function findUserMeta($user_id, $meta_key)
    {
        return UserMeta::firstOrNew(['user_id' => $user_id, 'meta_key' => $meta_key]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
