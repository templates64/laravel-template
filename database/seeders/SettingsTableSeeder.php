<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('admin.title');
        if ($setting->exists) {
            $setting->fill([
                'value'        => 'Antuvu',
            ])->save();
        }

        $setting = $this->findSetting('admin.description');
        if ($setting->exists) {
            $setting->fill([
                'value'        => __('seeders.settings.admin.description_value'),
            ])->save();
        }

        
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
