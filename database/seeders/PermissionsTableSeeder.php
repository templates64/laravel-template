<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $keys = [
            'browse_product',
            'browse_user_meta',
        ];

        foreach ($keys as $key) {
            Permission::firstOrCreate([
                'key'        => $key,
                'table_name' => null,
            ]);
        }

        Permission::generateFor('products');
        Permission::generateFor('user_metas');
    }
}
