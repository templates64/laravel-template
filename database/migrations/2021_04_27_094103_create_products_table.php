<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('author_id');
            $table->text('title');
            $table->text('slug');
            $table->double('price');
            $table->double('old_price')->nullable();
            $table->text('excerpt');
            $table->text('body');
            $table->double('rate')->default(5);
            $table->text('seo_title')->nullable();
            $table->text('image')->nullable();
            $table->text('images')->nullable();
            $table->text('meta_description')->nullable();
            $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING'])->default('DRAFT');
            $table->boolean('featured')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
