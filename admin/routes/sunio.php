<?php

use Illuminate\Support\Str;
use SIO\Sunio\Events\Routing;
use SIO\Sunio\Events\RoutingAdmin;
use SIO\Sunio\Events\RoutingAdminAfter;
use SIO\Sunio\Events\RoutingAfter;
use SIO\Sunio\Facades\Sunio;

/*
|--------------------------------------------------------------------------
| Sunio Routes
|--------------------------------------------------------------------------
|
| This file is where you may override any of the routes that are included
| with Sunio.
|
*/

Route::group(['as' => 'sunio.'], function () {
    event(new Routing());

    $namespacePrefix = '\\'.config('sunio.controllers.namespace').'\\';

    Route::get('login', ['uses' => $namespacePrefix.'SunioAuthController@login',     'as' => 'login']);
    Route::post('login', ['uses' => $namespacePrefix.'SunioAuthController@postLogin', 'as' => 'postlogin']);

    Route::group(['middleware' => 'admin.user'], function () use ($namespacePrefix) {
        event(new RoutingAdmin());

        // Main Admin and Logout Route
        Route::get('/', ['uses' => $namespacePrefix.'SunioController@index',   'as' => 'dashboard']);
        Route::post('logout', ['uses' => $namespacePrefix.'SunioController@logout',  'as' => 'logout']);
        Route::post('upload', ['uses' => $namespacePrefix.'SunioController@upload',  'as' => 'upload']);

        Route::get('profile', ['uses' => $namespacePrefix.'SunioUserController@profile', 'as' => 'profile']);

        try {
            foreach (Sunio::model('DataType')::all() as $dataType) {
                $breadController = $dataType->controller
                                 ? Str::start($dataType->controller, '\\')
                                 : $namespacePrefix.'SunioBaseController';

                Route::get($dataType->slug.'/order', $breadController.'@order')->name($dataType->slug.'.order');
                Route::post($dataType->slug.'/action', $breadController.'@action')->name($dataType->slug.'.action');
                Route::post($dataType->slug.'/order', $breadController.'@update_order')->name($dataType->slug.'.update_order');
                Route::get($dataType->slug.'/{id}/restore', $breadController.'@restore')->name($dataType->slug.'.restore');
                Route::get($dataType->slug.'/relation', $breadController.'@relation')->name($dataType->slug.'.relation');
                Route::post($dataType->slug.'/remove', $breadController.'@remove_media')->name($dataType->slug.'.media.remove');
                Route::resource($dataType->slug, $breadController, ['parameters' => [$dataType->slug => 'id']]);
            }
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException("Custom routes hasn't been configured because: ".$e->getMessage(), 1);
        } catch (\Exception $e) {
            // do nothing, might just be because table not yet migrated.
        }

        // Menu Routes
        Route::group([
            'as'     => 'menus.',
            'prefix' => 'menus/{menu}',
        ], function () use ($namespacePrefix) {
            Route::get('builder', ['uses' => $namespacePrefix.'SunioMenuController@builder',    'as' => 'builder']);
            Route::post('order', ['uses' => $namespacePrefix.'SunioMenuController@order_item', 'as' => 'order_item']);

            Route::group([
                'as'     => 'item.',
                'prefix' => 'item',
            ], function () use ($namespacePrefix) {
                Route::delete('{id}', ['uses' => $namespacePrefix.'SunioMenuController@delete_menu', 'as' => 'destroy']);
                Route::post('/', ['uses' => $namespacePrefix.'SunioMenuController@add_item',    'as' => 'add']);
                Route::put('/', ['uses' => $namespacePrefix.'SunioMenuController@update_item', 'as' => 'update']);
            });
        });

        // Settings
        Route::group([
            'as'     => 'settings.',
            'prefix' => 'settings',
        ], function () use ($namespacePrefix) {
            Route::get('/', ['uses' => $namespacePrefix.'SunioSettingsController@index',        'as' => 'index']);
            Route::post('/', ['uses' => $namespacePrefix.'SunioSettingsController@store',        'as' => 'store']);
            Route::put('/', ['uses' => $namespacePrefix.'SunioSettingsController@update',       'as' => 'update']);
            Route::delete('{id}', ['uses' => $namespacePrefix.'SunioSettingsController@delete',       'as' => 'delete']);
            Route::get('{id}/move_up', ['uses' => $namespacePrefix.'SunioSettingsController@move_up',      'as' => 'move_up']);
            Route::get('{id}/move_down', ['uses' => $namespacePrefix.'SunioSettingsController@move_down',    'as' => 'move_down']);
            Route::put('{id}/delete_value', ['uses' => $namespacePrefix.'SunioSettingsController@delete_value', 'as' => 'delete_value']);
        });

        // Admin Media
        Route::group([
            'as'     => 'media.',
            'prefix' => 'media',
        ], function () use ($namespacePrefix) {
            Route::get('/', ['uses' => $namespacePrefix.'SunioMediaController@index',              'as' => 'index']);
            Route::post('files', ['uses' => $namespacePrefix.'SunioMediaController@files',              'as' => 'files']);
            Route::post('new_folder', ['uses' => $namespacePrefix.'SunioMediaController@new_folder',         'as' => 'new_folder']);
            Route::post('delete_file_folder', ['uses' => $namespacePrefix.'SunioMediaController@delete', 'as' => 'delete']);
            Route::post('move_file', ['uses' => $namespacePrefix.'SunioMediaController@move',          'as' => 'move']);
            Route::post('rename_file', ['uses' => $namespacePrefix.'SunioMediaController@rename',        'as' => 'rename']);
            Route::post('upload', ['uses' => $namespacePrefix.'SunioMediaController@upload',             'as' => 'upload']);
            Route::post('crop', ['uses' => $namespacePrefix.'SunioMediaController@crop',             'as' => 'crop']);
        });

        // BREAD Routes
        Route::group([
            'as'     => 'bread.',
            'prefix' => 'bread',
        ], function () use ($namespacePrefix) {
            Route::get('/', ['uses' => $namespacePrefix.'SunioBreadController@index',              'as' => 'index']);
            Route::get('{table}/create', ['uses' => $namespacePrefix.'SunioBreadController@create',     'as' => 'create']);
            Route::post('/', ['uses' => $namespacePrefix.'SunioBreadController@store',   'as' => 'store']);
            Route::get('{table}/edit', ['uses' => $namespacePrefix.'SunioBreadController@edit', 'as' => 'edit']);
            Route::put('{id}', ['uses' => $namespacePrefix.'SunioBreadController@update',  'as' => 'update']);
            Route::delete('{id}', ['uses' => $namespacePrefix.'SunioBreadController@destroy',  'as' => 'delete']);
            Route::post('relationship', ['uses' => $namespacePrefix.'SunioBreadController@addRelationship',  'as' => 'relationship']);
            Route::get('delete_relationship/{id}', ['uses' => $namespacePrefix.'SunioBreadController@deleteRelationship',  'as' => 'delete_relationship']);
        });

        // Database Routes
        Route::resource('database', $namespacePrefix.'SunioDatabaseController');

        // Compass Routes
        Route::group([
            'as'     => 'compass.',
            'prefix' => 'compass',
        ], function () use ($namespacePrefix) {
            Route::get('/', ['uses' => $namespacePrefix.'SunioCompassController@index',  'as' => 'index']);
            Route::post('/', ['uses' => $namespacePrefix.'SunioCompassController@index',  'as' => 'post']);
        });

        event(new RoutingAdminAfter());
    });

    //Asset Routes
    Route::get('sunio-assets', ['uses' => $namespacePrefix.'SunioController@assets', 'as' => 'sunio_assets']);

    event(new RoutingAfter());
});
