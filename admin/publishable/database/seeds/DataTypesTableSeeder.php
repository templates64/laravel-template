<?php

use Illuminate\Database\Seeder;
use SIO\Sunio\Models\DataType;

class DataTypesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'users');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'users',
                'display_name_singular' => __('sunio::seeders.data_types.user.singular'),
                'display_name_plural'   => __('sunio::seeders.data_types.user.plural'),
                'icon'                  => 'sunio-person',
                'model_name'            => 'SIO\\Sunio\\Models\\User',
                'policy_name'           => 'SIO\\Sunio\\Policies\\UserPolicy',
                'controller'            => 'SIO\\Sunio\\Http\\Controllers\\SunioUserController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'menus');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'menus',
                'display_name_singular' => __('sunio::seeders.data_types.menu.singular'),
                'display_name_plural'   => __('sunio::seeders.data_types.menu.plural'),
                'icon'                  => 'sunio-list',
                'model_name'            => 'SIO\\Sunio\\Models\\Menu',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'roles');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'roles',
                'display_name_singular' => __('sunio::seeders.data_types.role.singular'),
                'display_name_plural'   => __('sunio::seeders.data_types.role.plural'),
                'icon'                  => 'sunio-lock',
                'model_name'            => 'SIO\\Sunio\\Models\\Role',
                'controller'            => 'SIO\\Sunio\\Http\\Controllers\\SunioRoleController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
