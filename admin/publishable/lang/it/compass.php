<?php

return [
    'welcome'                => 'Benvenuto in Sunio Compass. Every good app needs a compass to point them in the right direction.
    In this section you will find many resources and administrative tasks to help guide you as you build out your application.',
    'links'         => [
        'title'                 => 'Links',
        'documentation'         => 'Documentazione',
        'sunio_homepage'      => 'Sunio Homepage',
        'sunio_hooks'         => 'Sunio Hooks',
    ],
    'commands'      => [
        'title'                 => 'Comandi',
        'text'                  => 'Run Artisan Commands from Sunio.',
        'clear_output'          => 'clear output',
        'command_output'        => 'Artisan Command Output',
        'additional_args'       => 'Additional Args?',
        'run_command'           => 'Run Command',
    ],
    'resources'     => [
        'title'                 => 'Risorse',
        'text'                  => 'Sunio resources to help you find things quicker.',

    ],
    'logs'          => [
        'title'                 => 'Logs',
        'text'                  => 'Your app logs',
        'file_too_big'          => 'Log file >50M, please download it.',
        'level'                 => 'Level',
        'context'               => 'Context',
        'date'                  => 'Date',
        'content'               => 'Content',
        'download_file'         => 'Download file',
        'delete_file'           => 'Delete file',
        'delete_all_files'      => 'Delete all files',
        'delete_success'        => 'Successfully deleted log file:',
        'delete_all_success'    => 'Successfully deleted all log files',

    ],
    'fonts'         => [
        'title'                 => 'Fonts',
        'font_class'            => 'Sunio Fonts Class Mapping',
        'font_character'        => 'Sunio Fonts Character Mapping',
    ],
];
