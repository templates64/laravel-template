<?php

return [
    'loggingin'    => 'Вхід в систему',
    'signin_below' => 'Вхід тут:',
    'welcome'      => 'Ласкаво просимо до Sunio. Панель управління, якої не вистачало в Laravel',
];
