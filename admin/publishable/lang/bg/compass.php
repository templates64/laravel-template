<?php

return [
    'welcome'                => 'Добре дошли във Sunio Compass. Всяка хубава апликация има нужда от компас да насочва потребителите към правилната посока.

    В тази секция ще намерите много ресурси и административки задачи, които ще ви помогнат да създадете вашата апликация.',
    'links'         => [
        'title'                 => 'Линкове',
        'documentation'         => 'Документация',
        'sunio_homepage'      => 'Начална страница на Sunio',
        'sunio_hooks'         => 'Sunio Hooks',
    ],
    'commands'      => [
        'title'                 => 'Команди',
        'text'                  => 'Run Artisan команди от Sunio.',
        'clear_output'          => 'изчистване на информация ',
        'command_output'        => 'Artisan Command Output',
        'additional_args'       => 'Допълнителни аргументи?',
        'run_command'           => 'Изпълняване на коамнда',
    ],
    'resources'     => [
        'title'                 => 'Ресурси',
        'text'                  => 'Sunio ресурси, които ще ви помогнат да намерите всичко по-бързо.',

    ],
    'logs'          => [
        'title'                 => 'Логове',
        'text'                  => 'Логове на вашата апликация',
        'file_too_big'          => 'Лог файл >50M, моля свалето го.',
        'level'                 => 'Ниво',
        'context'               => 'Context',
        'date'                  => 'Дата',
        'content'               => 'Съдържание',
        'download_file'         => 'Сваляне на файл',
        'delete_file'           => 'Изтриване на файл',
        'delete_all_files'      => 'Изтриване на всички файлове',
        'delete_success'        => 'Успешно изтри лог файл:',
        'delete_all_success'    => 'Успешно изтрити всички лог файлове',

    ],
    'fonts'         => [
        'title'                 => 'Фонтове',
        'font_class'            => 'Sunio Fonts Class Mapping',
        'font_character'        => 'Sunio Fonts Character Mapping',
    ],
];
