<?php

namespace SIO\Sunio\Tests;

class RouteTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testGetRoutes()
    {
        $this->disableExceptionHandling();

        $this->visit(route('sunio.login'));
        $this->type('admin@admin.com', 'email');
        $this->type('password', 'password');
        $this->press(__('sunio::generic.login'));

        $urls = [
            route('sunio.dashboard'),
            route('sunio.media.index'),
            route('sunio.settings.index'),
            route('sunio.roles.index'),
            route('sunio.roles.create'),
            route('sunio.roles.show', 1),
            route('sunio.roles.edit', 1),
            route('sunio.users.index'),
            route('sunio.users.create'),
            route('sunio.users.show', 1),
            route('sunio.users.edit', 1),
            route('sunio.posts.index'),
            route('sunio.posts.create'),
            route('sunio.posts.show', 1),
            route('sunio.posts.edit', 1),
            route('sunio.pages.index'),
            route('sunio.pages.create'),
            route('sunio.pages.show', 1),
            route('sunio.pages.edit', 1),
            route('sunio.categories.index'),
            route('sunio.categories.create'),
            route('sunio.categories.show', 1),
            route('sunio.categories.edit', 1),
            route('sunio.menus.index'),
            route('sunio.menus.create'),
            route('sunio.menus.show', 1),
            route('sunio.menus.edit', 1),
            route('sunio.database.index'),
            route('sunio.bread.edit', 'categories'),
            route('sunio.database.edit', 'categories'),
            route('sunio.database.create'),
        ];

        foreach ($urls as $url) {
            $response = $this->call('GET', $url);
            $this->assertEquals(200, $response->status(), $url.' did not return a 200');
        }
    }
}
