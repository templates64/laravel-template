<?php

namespace SIO\Sunio\Tests;

use Illuminate\Support\Facades\Auth;

class LoginTest extends TestCase
{
    public function testSuccessfulLoginWithDefaultCredentials()
    {
        $this->visit(route('sunio.login'))
             ->type('admin@admin.com', 'email')
             ->type('password', 'password')
             ->press(__('sunio::generic.login'))
             ->seePageIs(route('sunio.dashboard'));
    }

    public function testShowAnErrorMessageWhenITryToLoginWithWrongCredentials()
    {
        session()->setPreviousUrl(route('sunio.login'));

        $this->visit(route('sunio.login'))
             ->type('john@Doe.com', 'email')
             ->type('pass', 'password')
             ->press(__('sunio::generic.login'))
             ->seePageIs(route('sunio.login'))
             ->see(__('auth.failed'))
             ->seeInField('email', 'john@Doe.com');
    }

    public function testRedirectIfLoggedIn()
    {
        Auth::loginUsingId(1);

        $this->visit(route('sunio.login'))
             ->seePageIs(route('sunio.dashboard'));
    }

    public function testRedirectIfNotLoggedIn()
    {
        $this->visit(route('sunio.profile'))
             ->seePageIs(route('sunio.login'));
    }

    public function testCanLogout()
    {
        Auth::loginUsingId(1);

        $this->visit(route('sunio.dashboard'))
             ->press(__('sunio::generic.logout'))
             ->seePageIs(route('sunio.login'));
    }

    public function testGetsLockedOutAfterFiveAttempts()
    {
        session()->setPreviousUrl(route('sunio.login'));

        for ($i = 0; $i <= 5; $i++) {
            $t = $this->visit(route('sunio.login'))
                 ->type('john@Doe.com', 'email')
                 ->type('pass', 'password')
                 ->press(__('sunio::generic.login'));
        }

        $t->see(__('auth.throttle', ['seconds' => 60]));
    }
}
