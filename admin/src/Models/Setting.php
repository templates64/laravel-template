<?php

namespace SIO\Sunio\Models;

use Illuminate\Database\Eloquent\Model;
use SIO\Sunio\Events\SettingUpdated;

class Setting extends Model
{
    protected $table = 'settings';

    protected $guarded = [];

    public $timestamps = false;

    protected $dispatchesEvents = [
        'updating' => SettingUpdated::class,
    ];
}
