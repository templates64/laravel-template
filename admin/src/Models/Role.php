<?php

namespace SIO\Sunio\Models;

use Illuminate\Database\Eloquent\Model;
use SIO\Sunio\Facades\Sunio;

class Role extends Model
{
    protected $guarded = [];

    public function users()
    {
        $userModel = Sunio::modelClass('User');

        return $this->belongsToMany($userModel, 'user_roles')
                    ->select(app($userModel)->getTable().'.*')
                    ->union($this->hasMany($userModel))->getQuery();
    }

    public function permissions()
    {
        return $this->belongsToMany(Sunio::modelClass('Permission'));
    }
}
