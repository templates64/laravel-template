<?php

namespace SIO\Sunio\Facades;

use Illuminate\Support\Facades\Facade;

class Sunio extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sunio';
    }
}
