<?php

namespace SIO\Sunio\Actions;

class ViewAction extends AbstractAction
{
    public function getTitle()
    {
        return __('sunio::generic.view');
    }

    public function getIcon()
    {
        return 'sunio-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-warning pull-right view',
        ];
    }

    public function getDefaultRoute()
    {
        return route('sunio.'.$this->dataType->slug.'.show', $this->data->{$this->data->getKeyName()});
    }
}
