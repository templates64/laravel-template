<?php

namespace SIO\Sunio\Actions;

class EditAction extends AbstractAction
{
    public function getTitle()
    {
        return __('sunio::generic.edit');
    }

    public function getIcon()
    {
        return 'sunio-edit';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right edit',
        ];
    }

    public function getDefaultRoute()
    {
        return route('sunio.'.$this->dataType->slug.'.edit', $this->data->{$this->data->getKeyName()});
    }
}
