<?php

namespace SIO\Sunio\Actions;

class RestoreAction extends AbstractAction
{
    public function getTitle()
    {
        return __('sunio::generic.restore');
    }

    public function getIcon()
    {
        return 'sunio-trash';
    }

    public function getPolicy()
    {
        return 'restore';
    }

    public function getAttributes()
    {
        return [
            'class'   => 'btn btn-sm btn-success pull-right restore',
            'data-id' => $this->data->{$this->data->getKeyName()},
            'id'      => 'restore-'.$this->data->{$this->data->getKeyName()},
        ];
    }

    public function getDefaultRoute()
    {
        return route('sunio.'.$this->dataType->slug.'.restore', $this->data->{$this->data->getKeyName()});
    }
}
