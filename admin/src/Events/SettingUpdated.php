<?php

namespace SIO\Sunio\Events;

use Illuminate\Queue\SerializesModels;
use SIO\Sunio\Models\Setting;

class SettingUpdated
{
    use SerializesModels;

    public $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }
}
