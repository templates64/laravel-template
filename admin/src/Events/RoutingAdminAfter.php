<?php

namespace SIO\Sunio\Events;

use Illuminate\Queue\SerializesModels;

class RoutingAdminAfter
{
    use SerializesModels;

    public $router;

    public function __construct()
    {
        $this->router = app('router');

        // @deprecate
        //
        event('sunio.admin.routing.after', $this->router);
    }
}
