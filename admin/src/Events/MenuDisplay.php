<?php

namespace SIO\Sunio\Events;

use Illuminate\Queue\SerializesModels;
use SIO\Sunio\Models\Menu;

class MenuDisplay
{
    use SerializesModels;

    public $menu;

    public function __construct(Menu $menu)
    {
        $this->menu = $menu;

        // @deprecate
        //
        event('sunio.menu.display', $menu);
    }
}
