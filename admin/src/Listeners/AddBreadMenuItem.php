<?php

namespace SIO\Sunio\Listeners;

use SIO\Sunio\Events\BreadAdded;
use SIO\Sunio\Facades\Sunio;

class AddBreadMenuItem
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Create a MenuItem for a given BREAD.
     *
     * @param BreadAdded $event
     *
     * @return void
     */
    public function handle(BreadAdded $bread)
    {
        if (config('sunio.bread.add_menu_item') && file_exists(base_path('routes/web.php'))) {
            $menu = Sunio::model('Menu')->where('name', config('sunio.bread.default_menu'))->firstOrFail();

            $menuItem = Sunio::model('MenuItem')->firstOrNew([
                'menu_id' => $menu->id,
                'title'   => $bread->dataType->getTranslatedAttribute('display_name_plural'),
                'url'     => '',
                'route'   => 'sunio.'.$bread->dataType->slug.'.index',
            ]);

            $order = Sunio::model('MenuItem')->highestOrderMenuItem();

            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => $bread->dataType->icon,
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => $order,
                ])->save();
            }
        }
    }
}
