<?php

namespace SIO\Sunio\Listeners;

use SIO\Sunio\Events\BreadAdded;
use SIO\Sunio\Facades\Sunio;

class AddBreadPermission
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Create Permission for a given BREAD.
     *
     * @param BreadAdded $event
     *
     * @return void
     */
    public function handle(BreadAdded $bread)
    {
        if (config('sunio.bread.add_permission') && file_exists(base_path('routes/web.php'))) {
            // Create permission
            //
            // Permission::generateFor(Str::snake($bread->dataType->slug));
            $role = Sunio::model('Role')->where('name', config('sunio.bread.default_role'))->firstOrFail();

            // Get permission for added table
            $permissions = Sunio::model('Permission')->where(['table_name' => $bread->dataType->name])->get()->pluck('id')->all();

            // Assign permission to admin
            $role->permissions()->attach($permissions);
        }
    }
}
