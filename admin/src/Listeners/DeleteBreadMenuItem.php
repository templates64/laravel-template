<?php

namespace SIO\Sunio\Listeners;

use SIO\Sunio\Events\BreadDeleted;
use SIO\Sunio\Facades\Sunio;

class DeleteBreadMenuItem
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Delete a MenuItem for a given BREAD.
     *
     * @param BreadDeleted $bread
     *
     * @return void
     */
    public function handle(BreadDeleted $bread)
    {
        if (config('sunio.bread.add_menu_item')) {
            $menuItem = Sunio::model('MenuItem')->where('route', 'sunio.'.$bread->dataType->slug.'.index');

            if ($menuItem->exists()) {
                $menuItem->delete();
            }
        }
    }
}
