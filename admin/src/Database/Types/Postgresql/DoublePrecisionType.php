<?php

namespace SIO\Sunio\Database\Types\Postgresql;

use SIO\Sunio\Database\Types\Common\DoubleType;

class DoublePrecisionType extends DoubleType
{
    public const NAME = 'double precision';
    public const DBTYPE = 'float8';
}
