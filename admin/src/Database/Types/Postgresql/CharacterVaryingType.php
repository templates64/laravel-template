<?php

namespace SIO\Sunio\Database\Types\Postgresql;

use SIO\Sunio\Database\Types\Common\VarCharType;

class CharacterVaryingType extends VarCharType
{
    public const NAME = 'character varying';
    public const DBTYPE = 'varchar';
}
