<?php

namespace SIO\Sunio\Database\Types\Postgresql;

use SIO\Sunio\Database\Types\Common\CharType;

class CharacterType extends CharType
{
    public const NAME = 'character';
    public const DBTYPE = 'bpchar';
}
