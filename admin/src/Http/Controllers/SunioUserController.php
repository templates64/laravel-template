<?php

namespace SIO\Sunio\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SIO\Sunio\Facades\Sunio;

class SunioUserController extends SunioBaseController
{
    public function profile(Request $request)
    {
        $route = '';
        $dataType = Sunio::model('DataType')->where('model_name', Auth::guard(app('SunioGuard'))->getProvider()->getModel())->first();
        if (!$dataType && app('SunioGuard') == 'web') {
            $route = route('sunio.users.edit', Auth::user()->getKey());
        } elseif ($dataType) {
            $route = route('sunio.'.$dataType->slug.'.edit', Auth::user()->getKey());
        }

        return Sunio::view('sunio::profile', compact('route'));
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        if (Auth::user()->getKey() == $id) {
            $request->merge([
                'role_id'                              => Auth::user()->role_id,
                'user_belongstomany_role_relationship' => Auth::user()->roles->pluck('id')->toArray(),
            ]);
        }

        return parent::update($request, $id);
    }
}
