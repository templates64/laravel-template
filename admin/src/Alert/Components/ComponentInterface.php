<?php

namespace SIO\Sunio\Alert\Components;

interface ComponentInterface
{
    public function render();
}
