<?php

namespace SIO\Sunio\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use SIO\Sunio\Facades\Sunio;

class PostDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Sunio::model('Post')->count();
        $string = trans_choice('sunio::dimmer.post', $count);

        return view('sunio::dimmer', array_merge($this->config, [
            'icon'   => 'sunio-news',
            'title'  => "{$count} {$string}",
            'text'   => __('sunio::dimmer.post_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('sunio::dimmer.post_link_text'),
                'link' => route('sunio.posts.index'),
            ],
            'image' => sunio_asset('images/widget-backgrounds/02.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Sunio::model('Post'));
    }
}
