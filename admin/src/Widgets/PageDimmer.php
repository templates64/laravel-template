<?php

namespace SIO\Sunio\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use SIO\Sunio\Facades\Sunio;

class PageDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Sunio::model('Page')->count();
        $string = trans_choice('sunio::dimmer.page', $count);

        return view('sunio::dimmer', array_merge($this->config, [
            'icon'   => 'sunio-file-text',
            'title'  => "{$count} {$string}",
            'text'   => __('sunio::dimmer.page_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('sunio::dimmer.page_link_text'),
                'link' => route('sunio.pages.index'),
            ],
            'image' => sunio_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Sunio::model('Page'));
    }
}
