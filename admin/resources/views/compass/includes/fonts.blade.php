<h2>{{ __('sunio::compass.fonts.font_class') }}</h2>
<ul class="glyphs css-mapping">
    <li>
        <div class="icon sunio-bread"></div>
        <input type="text" readonly="readonly" value="sunio-bread">
    </li>
    <li>
        <div class="icon sunio-helm"></div>
        <input type="text" readonly="readonly" value="sunio-helm">
    </li>
    <li>
        <div class="icon sunio-play"></div>
        <input type="text" readonly="readonly" value="sunio-play">
    </li>
    <li>
        <div class="icon sunio-activity"></div>
        <input type="text" readonly="readonly" value="sunio-activity">
    </li>
    <li>
        <div class="icon sunio-company"></div>
        <input type="text" readonly="readonly" value="sunio-company">
    </li>
    <li>
        <div class="icon sunio-file-text"></div>
        <input type="text" readonly="readonly" value="sunio-file-text">
    </li>
    <li>
        <div class="icon sunio-folder"></div>
        <input type="text" readonly="readonly" value="sunio-folder">
    </li>
    <li>
        <div class="icon sunio-paw"></div>
        <input type="text" readonly="readonly" value="sunio-paw">
    </li>
    <li>
        <div class="icon sunio-sort"></div>
        <input type="text" readonly="readonly" value="sunio-sort">
    </li>
    <li>
        <div class="icon sunio-sort-asc"></div>
        <input type="text" readonly="readonly" value="sunio-sort-asc">
    </li>
    <li>
        <div class="icon sunio-sort-desc"></div>
        <input type="text" readonly="readonly" value="sunio-sort-desc">
    </li>
    <li>
        <div class="icon sunio-handle"></div>
        <input type="text" readonly="readonly" value="sunio-handle">
    </li>
    <li>
        <div class="icon sunio-hammer"></div>
        <input type="text" readonly="readonly" value="sunio-hammer">
    </li>
    <li>
        <div class="icon sunio-images"></div>
        <input type="text" readonly="readonly" value="sunio-images">
    </li>
    <li>
        <div class="icon sunio-categories"></div>
        <input type="text" readonly="readonly" value="sunio-categories">
    </li>
    <li>
        <div class="icon sunio-group"></div>
        <input type="text" readonly="readonly" value="sunio-group">
    </li>
    <li>
        <div class="icon sunio-person"></div>
        <input type="text" readonly="readonly" value="sunio-person">
    </li>
    <li>
        <div class="icon sunio-people"></div>
        <input type="text" readonly="readonly" value="sunio-people">
    </li>
    <li>
        <div class="icon sunio-power"></div>
        <input type="text" readonly="readonly" value="sunio-power">
    </li>
    <li>
        <div class="icon sunio-tools"></div>
        <input type="text" readonly="readonly" value="sunio-tools">
    </li>
    <li>
        <div class="icon sunio-anchor"></div>
        <input type="text" readonly="readonly" value="sunio-anchor">
    </li>
    <li>
        <div class="icon sunio-double-down"></div>
        <input type="text" readonly="readonly" value="sunio-double-down">
    </li>
    <li>
        <div class="icon sunio-double-up"></div>
        <input type="text" readonly="readonly" value="sunio-double-up">
    </li>
    <li>
        <div class="icon sunio-double-left"></div>
        <input type="text" readonly="readonly" value="sunio-double-left">
    </li>
    <li>
        <div class="icon sunio-double-right"></div>
        <input type="text" readonly="readonly" value="sunio-double-right">
    </li>
    <li>
        <div class="icon sunio-window-list"></div>
        <input type="text" readonly="readonly" value="sunio-window-list">
    </li>
    <li>
        <div class="icon sunio-x"></div>
        <input type="text" readonly="readonly" value="sunio-x">
    </li>
    <li>
        <div class="icon sunio-dot"></div>
        <input type="text" readonly="readonly" value="sunio-dot">
    </li>
    <li>
        <div class="icon sunio-dot-2"></div>
        <input type="text" readonly="readonly" value="sunio-dot-2">
    </li>
    <li>
        <div class="icon sunio-dot-3"></div>
        <input type="text" readonly="readonly" value="sunio-dot-3">
    </li>
    <li>
        <div class="icon sunio-list"></div>
        <input type="text" readonly="readonly" value="sunio-list">
    </li>
    <li>
        <div class="icon sunio-list-add"></div>
        <input type="text" readonly="readonly" value="sunio-list-add">
    </li>
    <li>
        <div class="icon sunio-pie-chart"></div>
        <input type="text" readonly="readonly" value="sunio-pie-chart">
    </li>
    <li>
        <div class="icon sunio-watch"></div>
        <input type="text" readonly="readonly" value="sunio-watch">
    </li>
    <li>
        <div class="icon sunio-data"></div>
        <input type="text" readonly="readonly" value="sunio-data">
    </li>
    <li>
        <div class="icon sunio-music"></div>
        <input type="text" readonly="readonly" value="sunio-music">
    </li>
    <li>
        <div class="icon sunio-settings"></div>
        <input type="text" readonly="readonly" value="sunio-settings">
    </li>
    <li>
        <div class="icon sunio-video"></div>
        <input type="text" readonly="readonly" value="sunio-video">
    </li>
    <li>
        <div class="icon sunio-trash"></div>
        <input type="text" readonly="readonly" value="sunio-trash">
    </li>
    <li>
        <div class="icon sunio-tv"></div>
        <input type="text" readonly="readonly" value="sunio-tv">
    </li>
    <li>
        <div class="icon sunio-lock"></div>
        <input type="text" readonly="readonly" value="sunio-lock">
    </li>
    <li>
        <div class="icon sunio-news"></div>
        <input type="text" readonly="readonly" value="sunio-news">
    </li>
    <li>
        <div class="icon sunio-bag"></div>
        <input type="text" readonly="readonly" value="sunio-bag">
    </li>
    <li>
        <div class="icon sunio-upload"></div>
        <input type="text" readonly="readonly" value="sunio-upload">
    </li>
    <li>
        <div class="icon sunio-refresh"></div>
        <input type="text" readonly="readonly" value="sunio-refresh">
    </li>
    <li>
        <div class="icon sunio-character"></div>
        <input type="text" readonly="readonly" value="sunio-character">
    </li>
    <li>
        <div class="icon sunio-move"></div>
        <input type="text" readonly="readonly" value="sunio-move">
    </li>
    <li>
        <div class="icon sunio-paypal"></div>
        <input type="text" readonly="readonly" value="sunio-paypal">
    </li>
    <li>
        <div class="icon sunio-paper-plane"></div>
        <input type="text" readonly="readonly" value="sunio-paper-plane">
    </li>
    <li>
        <div class="icon sunio-ticket"></div>
        <input type="text" readonly="readonly" value="sunio-ticket">
    </li>
    <li>
        <div class="icon sunio-youtube-play"></div>
        <input type="text" readonly="readonly" value="sunio-youtube-play">
    </li>
    <li>
        <div class="icon sunio-boat"></div>
        <input type="text" readonly="readonly" value="sunio-boat">
    </li>
    <li>
        <div class="icon sunio-check"></div>
        <input type="text" readonly="readonly" value="sunio-check">
    </li>
    <li>
        <div class="icon sunio-alarm-clock"></div>
        <input type="text" readonly="readonly" value="sunio-alarm-clock">
    </li>
    <li>
        <div class="icon sunio-controller"></div>
        <input type="text" readonly="readonly" value="sunio-controller">
    </li>
    <li>
        <div class="icon sunio-pizza"></div>
        <input type="text" readonly="readonly" value="sunio-pizza">
    </li>
    <li>
        <div class="icon sunio-backpack"></div>
        <input type="text" readonly="readonly" value="sunio-backpack">
    </li>
    <li>
        <div class="icon sunio-barbeque"></div>
        <input type="text" readonly="readonly" value="sunio-barbeque">
    </li>
    <li>
        <div class="icon sunio-bomb"></div>
        <input type="text" readonly="readonly" value="sunio-bomb">
    </li>
    <li>
        <div class="icon sunio-book-download"></div>
        <input type="text" readonly="readonly" value="sunio-book-download">
    </li>
    <li>
        <div class="icon sunio-browser"></div>
        <input type="text" readonly="readonly" value="sunio-browser">
    </li>
    <li>
        <div class="icon sunio-brush"></div>
        <input type="text" readonly="readonly" value="sunio-brush">
    </li>
    <li>
        <div class="icon sunio-bubble-hear"></div>
        <input type="text" readonly="readonly" value="sunio-bubble-hear">
    </li>
    <li>
        <div class="icon sunio-polaroid"></div>
        <input type="text" readonly="readonly" value="sunio-polaroid">
    </li>
    <li>
        <div class="icon sunio-crop"></div>
        <input type="text" readonly="readonly" value="sunio-crop">
    </li>
    <li>
        <div class="icon sunio-dashboard"></div>
        <input type="text" readonly="readonly" value="sunio-dashboard">
    </li>
    <li>
        <div class="icon sunio-hotdog"></div>
        <input type="text" readonly="readonly" value="sunio-hotdog">
    </li>
    <li>
        <div class="icon sunio-laptop"></div>
        <input type="text" readonly="readonly" value="sunio-laptop">
    </li>
    <li>
        <div class="icon sunio-paint-bucket"></div>
        <input type="text" readonly="readonly" value="sunio-paint-bucket">
    </li>
    <li>
        <div class="icon sunio-photo"></div>
        <input type="text" readonly="readonly" value="sunio-photo">
    </li>
    <li>
        <div class="icon sunio-photos"></div>
        <input type="text" readonly="readonly" value="sunio-photos">
    </li>
    <li>
        <div class="icon sunio-receipt"></div>
        <input type="text" readonly="readonly" value="sunio-receipt">
    </li>
    <li>
        <div class="icon sunio-buy"></div>
        <input type="text" readonly="readonly" value="sunio-buy">
    </li>
    <li>
        <div class="icon sunio-lightbulb"></div>
        <input type="text" readonly="readonly" value="sunio-lightbulb">
    </li>
    <li>
        <div class="icon sunio-archive"></div>
        <input type="text" readonly="readonly" value="sunio-archive">
    </li>
    <li>
        <div class="icon sunio-paperclip"></div>
        <input type="text" readonly="readonly" value="sunio-paperclip">
    </li>
    <li>
        <div class="icon sunio-warning"></div>
        <input type="text" readonly="readonly" value="sunio-warning">
    </li>
    <li>
        <div class="icon sunio-basket"></div>
        <input type="text" readonly="readonly" value="sunio-basket">
    </li>
    <li>
        <div class="icon sunio-bell"></div>
        <input type="text" readonly="readonly" value="sunio-bell">
    </li>
    <li>
        <div class="icon sunio-book"></div>
        <input type="text" readonly="readonly" value="sunio-book">
    </li>
    <li>
        <div class="icon sunio-chat"></div>
        <input type="text" readonly="readonly" value="sunio-chat">
    </li>
    <li>
        <div class="icon sunio-down-circled"></div>
        <input type="text" readonly="readonly" value="sunio-down-circled">
    </li>
    <li>
        <div class="icon sunio-location"></div>
        <input type="text" readonly="readonly" value="sunio-location">
    </li>
    <li>
        <div class="icon sunio-forward"></div>
        <input type="text" readonly="readonly" value="sunio-forward">
    </li>
    <li>
        <div class="icon sunio-flashlight"></div>
        <input type="text" readonly="readonly" value="sunio-flashlight">
    </li>
    <li>
        <div class="icon sunio-trophy"></div>
        <input type="text" readonly="readonly" value="sunio-trophy">
    </li>
    <li>
        <div class="icon sunio-diamond"></div>
        <input type="text" readonly="readonly" value="sunio-diamond">
    </li>
    <li>
        <div class="icon sunio-credit-cards"></div>
        <input type="text" readonly="readonly" value="sunio-credit-cards">
    </li>
    <li>
        <div class="icon sunio-shop"></div>
        <input type="text" readonly="readonly" value="sunio-shop">
    </li>
    <li>
        <div class="icon sunio-sound"></div>
        <input type="text" readonly="readonly" value="sunio-sound">
    </li>
    <li>
        <div class="icon sunio-study"></div>
        <input type="text" readonly="readonly" value="sunio-study">
    </li>
    <li>
        <div class="icon sunio-pen"></div>
        <input type="text" readonly="readonly" value="sunio-pen">
    </li>
    <li>
        <div class="icon sunio-params"></div>
        <input type="text" readonly="readonly" value="sunio-params">
    </li>
    <li>
        <div class="icon sunio-fire"></div>
        <input type="text" readonly="readonly" value="sunio-fire">
    </li>
    <li>
        <div class="icon sunio-dollar"></div>
        <input type="text" readonly="readonly" value="sunio-dollar">
    </li>
    <li>
        <div class="icon sunio-bulb"></div>
        <input type="text" readonly="readonly" value="sunio-bulb">
    </li>
    <li>
        <div class="icon sunio-lab"></div>
        <input type="text" readonly="readonly" value="sunio-lab">
    </li>
    <li>
        <div class="icon sunio-cup"></div>
        <input type="text" readonly="readonly" value="sunio-cup">
    </li>
    <li>
        <div class="icon sunio-megaphone"></div>
        <input type="text" readonly="readonly" value="sunio-megaphone">
    </li>
    <li>
        <div class="icon sunio-heart"></div>
        <input type="text" readonly="readonly" value="sunio-heart">
    </li>
    <li>
        <div class="icon sunio-bubble"></div>
        <input type="text" readonly="readonly" value="sunio-bubble">
    </li>
    <li>
        <div class="icon sunio-search"></div>
        <input type="text" readonly="readonly" value="sunio-search">
    </li>
    <li>
        <div class="icon sunio-tag"></div>
        <input type="text" readonly="readonly" value="sunio-tag">
    </li>
    <li>
        <div class="icon sunio-world"></div>
        <input type="text" readonly="readonly" value="sunio-world">
    </li>
    <li>
        <div class="icon sunio-camera"></div>
        <input type="text" readonly="readonly" value="sunio-camera">
    </li>
    <li>
        <div class="icon sunio-calendar"></div>
        <input type="text" readonly="readonly" value="sunio-calendar">
    </li>
    <li>
        <div class="icon sunio-mail"></div>
        <input type="text" readonly="readonly" value="sunio-mail">
    </li>
    <li>
        <div class="icon sunio-phone"></div>
        <input type="text" readonly="readonly" value="sunio-phone">
    </li>
    <li>
        <div class="icon sunio-star"></div>
        <input type="text" readonly="readonly" value="sunio-star">
    </li>
    <li>
        <div class="icon sunio-star-half"></div>
        <input type="text" readonly="readonly" value="sunio-star-half">
    </li>
    <li>
        <div class="icon sunio-star-half-1"></div>
        <input type="text" readonly="readonly" value="sunio-star-half-1">
    </li>
    <li>
        <div class="icon sunio-star-two"></div>
        <input type="text" readonly="readonly" value="sunio-star-two">
    </li>
    <li>
        <div class="icon sunio-medal-rank-star"></div>
        <input type="text" readonly="readonly" value="sunio-medal-rank-star">
    </li>
    <li>
        <div class="icon sunio-facebook"></div>
        <input type="text" readonly="readonly" value="sunio-facebook">
    </li>
    <li>
        <div class="icon sunio-google-plus"></div>
        <input type="text" readonly="readonly" value="sunio-google-plus">
    </li>
    <li>
        <div class="icon sunio-twitter"></div>
        <input type="text" readonly="readonly" value="sunio-twitter">
    </li>
    <li>
        <div class="icon sunio-github"></div>
        <input type="text" readonly="readonly" value="sunio-github">
    </li>
    <li>
        <div class="icon sunio-code"></div>
        <input type="text" readonly="readonly" value="sunio-code">
    </li>
    <li>
        <div class="icon sunio-pie-graph"></div>
        <input type="text" readonly="readonly" value="sunio-pie-graph">
    </li>
    <li>
        <div class="icon sunio-javascript"></div>
        <input type="text" readonly="readonly" value="sunio-javascript">
    </li>
    <li>
        <div class="icon sunio-wand"></div>
        <input type="text" readonly="readonly" value="sunio-wand">
    </li>
    <li>
        <div class="icon sunio-wineglass"></div>
        <input type="text" readonly="readonly" value="sunio-wineglass">
    </li>
    <li>
        <div class="icon sunio-milestone"></div>
        <input type="text" readonly="readonly" value="sunio-milestone">
    </li>
    <li>
        <div class="icon sunio-terminal"></div>
        <input type="text" readonly="readonly" value="sunio-terminal">
    </li>
    <li>
        <div class="icon sunio-plug"></div>
        <input type="text" readonly="readonly" value="sunio-plug">
    </li>
    <li>
        <div class="icon sunio-gift"></div>
        <input type="text" readonly="readonly" value="sunio-gift">
    </li>
    <li>
        <div class="icon sunio-markdown"></div>
        <input type="text" readonly="readonly" value="sunio-markdown">
    </li>
    <li>
        <div class="icon sunio-beer"></div>
        <input type="text" readonly="readonly" value="sunio-beer">
    </li>
    <li>
        <div class="icon sunio-credit-card"></div>
        <input type="text" readonly="readonly" value="sunio-credit-card">
    </li>
    <li>
        <div class="icon sunio-bolt"></div>
        <input type="text" readonly="readonly" value="sunio-bolt">
    </li>
    <li>
        <div class="icon sunio-external"></div>
        <input type="text" readonly="readonly" value="sunio-external">
    </li>
    <li>
        <div class="icon sunio-magnet"></div>
        <input type="text" readonly="readonly" value="sunio-magnet">
    </li>
    <li>
        <div class="icon sunio-certificate"></div>
        <input type="text" readonly="readonly" value="sunio-certificate">
    </li>
    <li>
        <div class="icon sunio-cloud-download"></div>
        <input type="text" readonly="readonly" value="sunio-cloud-download">
    </li>
    <li>
        <div class="icon sunio-campfire"></div>
        <input type="text" readonly="readonly" value="sunio-campfire">
    </li>
    <li>
        <div class="icon sunio-new"></div>
        <input type="text" readonly="readonly" value="sunio-new">
    </li>
    <li>
        <div class="icon sunio-skull"></div>
        <input type="text" readonly="readonly" value="sunio-skull">
    </li>
    <li>
        <div class="icon sunio-telephone"></div>
        <input type="text" readonly="readonly" value="sunio-telephone">
    </li>
    <li>
        <div class="icon sunio-trees"></div>
        <input type="text" readonly="readonly" value="sunio-trees">
    </li>
    <li>
        <div class="icon sunio-bar-chart"></div>
        <input type="text" readonly="readonly" value="sunio-bar-chart">
    </li>
    <li>
        <div class="icon sunio-check-circle"></div>
        <input type="text" readonly="readonly" value="sunio-check-circle">
    </li>
    <li>
        <div class="icon sunio-exclamation"></div>
        <input type="text" readonly="readonly" value="sunio-exclamation">
    </li>
    <li>
        <div class="icon sunio-font"></div>
        <input type="text" readonly="readonly" value="sunio-font">
    </li>
    <li>
        <div class="icon sunio-puzzle"></div>
        <input type="text" readonly="readonly" value="sunio-puzzle">
    </li>
    <li>
        <div class="icon sunio-smile"></div>
        <input type="text" readonly="readonly" value="sunio-smile">
    </li>
    <li>
        <div class="icon sunio-frown"></div>
        <input type="text" readonly="readonly" value="sunio-frown">
    </li>
    <li>
        <div class="icon sunio-meh"></div>
        <input type="text" readonly="readonly" value="sunio-meh">
    </li>
    <li>
        <div class="icon sunio-leaf"></div>
        <input type="text" readonly="readonly" value="sunio-leaf">
    </li>
    <li>
        <div class="icon sunio-info-circled"></div>
        <input type="text" readonly="readonly" value="sunio-info-circled">
    </li>
    <li>
        <div class="icon sunio-underline"></div>
        <input type="text" readonly="readonly" value="sunio-underline">
    </li>
    <li>
        <div class="icon sunio-wallet"></div>
        <input type="text" readonly="readonly" value="sunio-wallet">
    </li>
    <li>
        <div class="icon sunio-truck"></div>
        <input type="text" readonly="readonly" value="sunio-truck">
    </li>
    <li>
        <div class="icon sunio-eye"></div>
        <input type="text" readonly="readonly" value="sunio-eye">
    </li>
    <li>
        <div class="icon sunio-sun"></div>
        <input type="text" readonly="readonly" value="sunio-sun">
    </li>
    <li>
        <div class="icon sunio-barbell"></div>
        <input type="text" readonly="readonly" value="sunio-barbell">
    </li>
    <li>
        <div class="icon sunio-bookmark"></div>
        <input type="text" readonly="readonly" value="sunio-bookmark">
    </li>
    <li>
        <div class="icon sunio-volume-up"></div>
        <input type="text" readonly="readonly" value="sunio-volume-up">
    </li>
    <li>
        <div class="icon sunio-volume-down"></div>
        <input type="text" readonly="readonly" value="sunio-volume-down">
    </li>
    <li>
        <div class="icon sunio-wifi"></div>
        <input type="text" readonly="readonly" value="sunio-wifi">
    </li>
    <li>
        <div class="icon sunio-pause"></div>
        <input type="text" readonly="readonly" value="sunio-pause">
    </li>
    <li>
        <div class="icon sunio-resize-full"></div>
        <input type="text" readonly="readonly" value="sunio-resize-full">
    </li>
    <li>
        <div class="icon sunio-resize-small"></div>
        <input type="text" readonly="readonly" value="sunio-resize-small">
    </li>
    <li>
        <div class="icon sunio-download"></div>
        <input type="text" readonly="readonly" value="sunio-download">
    </li>
    <li>
        <div class="icon sunio-droplet"></div>
        <input type="text" readonly="readonly" value="sunio-droplet">
    </li>
    <li>
        <div class="icon sunio-umbrella"></div>
        <input type="text" readonly="readonly" value="sunio-umbrella">
    </li>
    <li>
        <div class="icon sunio-harddrive"></div>
        <input type="text" readonly="readonly" value="sunio-harddrive">
    </li>
    <li>
        <div class="icon sunio-css3"></div>
        <input type="text" readonly="readonly" value="sunio-css3">
    </li>
    <li>
        <div class="icon sunio-html5"></div>
        <input type="text" readonly="readonly" value="sunio-html5">
    </li>
    <li>
        <div class="icon sunio-tree"></div>
        <input type="text" readonly="readonly" value="sunio-tree">
    </li>
    <li>
        <div class="icon sunio-file-code"></div>
        <input type="text" readonly="readonly" value="sunio-file-code">
    </li>
    <li>
        <div class="icon sunio-bug"></div>
        <input type="text" readonly="readonly" value="sunio-bug">
    </li>
    <li>
        <div class="icon sunio-rocket"></div>
        <input type="text" readonly="readonly" value="sunio-rocket">
    </li>
    <li>
        <div class="icon sunio-key"></div>
        <input type="text" readonly="readonly" value="sunio-key">
    </li>
    <li>
        <div class="icon sunio-question"></div>
        <input type="text" readonly="readonly" value="sunio-question">
    </li>
    <li>
        <div class="icon sunio-cursor"></div>
        <input type="text" readonly="readonly" value="sunio-cursor">
    </li>
    <li>
        <div class="icon sunio-lifebuoy"></div>
        <input type="text" readonly="readonly" value="sunio-lifebuoy">
    </li>
    <li>
        <div class="icon sunio-thumbs-up"></div>
        <input type="text" readonly="readonly" value="sunio-thumbs-up">
    </li>
    <li>
        <div class="icon sunio-thumb-tack"></div>
        <input type="text" readonly="readonly" value="sunio-thumb-tack">
    </li>
    <li>
        <div class="icon sunio-edit"></div>
        <input type="text" readonly="readonly" value="sunio-edit">
    </li>
    <li>
        <div class="icon sunio-angle-down"></div>
        <input type="text" readonly="readonly" value="sunio-angle-down">
    </li>
    <li>
        <div class="icon sunio-angle-left"></div>
        <input type="text" readonly="readonly" value="sunio-angle-left">
    </li>
    <li>
        <div class="icon sunio-angle-right"></div>
        <input type="text" readonly="readonly" value="sunio-angle-right">
    </li>
    <li>
        <div class="icon sunio-angle-up"></div>
        <input type="text" readonly="readonly" value="sunio-angle-up">
    </li>
    <li>
        <div class="icon sunio-home"></div>
        <input type="text" readonly="readonly" value="sunio-home">
    </li>
    <li>
        <div class="icon sunio-pirate-swords"></div>
        <input type="text" readonly="readonly" value="sunio-pirate-swords">
    </li>
    <li>
        <div class="icon sunio-pirate"></div>
        <input type="text" readonly="readonly" value="sunio-pirate">
    </li>
    <li>
        <div class="icon sunio-rum"></div>
        <input type="text" readonly="readonly" value="sunio-rum">
    </li>
    <li>
        <div class="icon sunio-cannon"></div>
        <input type="text" readonly="readonly" value="sunio-cannon">
    </li>
    <li>
        <div class="icon sunio-hook"></div>
        <input type="text" readonly="readonly" value="sunio-hook">
    </li>
    <li>
        <div class="icon sunio-kraken"></div>
        <input type="text" readonly="readonly" value="sunio-kraken">
    </li>
    <li>
        <div class="icon sunio-lighthouse"></div>
        <input type="text" readonly="readonly" value="sunio-lighthouse">
    </li>
    <li>
        <div class="icon sunio-pirate-hat"></div>
        <input type="text" readonly="readonly" value="sunio-pirate-hat">
    </li>
    <li>
        <div class="icon sunio-rum-1"></div>
        <input type="text" readonly="readonly" value="sunio-rum-1">
    </li>
    <li>
        <div class="icon sunio-ship"></div>
        <input type="text" readonly="readonly" value="sunio-ship">
    </li>
    <li>
        <div class="icon sunio-treasure"></div>
        <input type="text" readonly="readonly" value="sunio-treasure">
    </li>
    <li>
        <div class="icon sunio-treasure-open"></div>
        <input type="text" readonly="readonly" value="sunio-treasure-open">
    </li>
    <li>
        <div class="icon sunio-whale"></div>
        <input type="text" readonly="readonly" value="sunio-whale">
    </li>
    <li>
        <div class="icon sunio-compass"></div>
        <input type="text" readonly="readonly" value="sunio-compass">
    </li>
    <li>
        <div class="icon sunio-logbook"></div>
        <input type="text" readonly="readonly" value="sunio-logbook">
    </li>
    <li>
        <div class="icon sunio-plus"></div>
        <input type="text" readonly="readonly" value="sunio-plus">
    </li>
    <li>
        <div class="icon sunio-documentation"></div>
        <input type="text" readonly="readonly" value="sunio-documentation">
    </li>
    <li>
        <div class="icon sunio-belt"></div>
        <input type="text" readonly="readonly" value="sunio-belt">
    </li>
    <li>
        <div class="icon sunio-zoom-in"></div>
        <input type="text" readonly="readonly" value="sunio-zoom-in">
    </li>
    <li>
        <div class="icon sunio-zoom-out"></div>
        <input type="text" readonly="readonly" value="sunio-zoom-out">
    </li>
    <li>
        <div class="icon sunio-scissors"></div>
        <input type="text" readonly="readonly" value="sunio-scissors">
    </li>
    <li>
        <div class="icon sunio-github-icon"></div>
        <input type="text" readonly="readonly" value="sunio-github-icon">
    </li>
</ul>

<h2>{{ __('sunio::compass.fonts.font_character') }}</h2>
<ul class="glyphs character-mapping">
    <li>
        <div data-icon="a" class="icon"></div>
        <input type="text" readonly="readonly" value="a">
    </li>
    <li>
        <div data-icon="b" class="icon"></div>
        <input type="text" readonly="readonly" value="b">
    </li>
    <li>
        <div data-icon="c" class="icon"></div>
        <input type="text" readonly="readonly" value="c">
    </li>
    <li>
        <div data-icon="d" class="icon"></div>
        <input type="text" readonly="readonly" value="d">
    </li>
    <li>
        <div data-icon="e" class="icon"></div>
        <input type="text" readonly="readonly" value="e">
    </li>
    <li>
        <div data-icon="f" class="icon"></div>
        <input type="text" readonly="readonly" value="f">
    </li>
    <li>
        <div data-icon="g" class="icon"></div>
        <input type="text" readonly="readonly" value="g">
    </li>
    <li>
        <div data-icon="h" class="icon"></div>
        <input type="text" readonly="readonly" value="h">
    </li>
    <li>
        <div data-icon="o" class="icon"></div>
        <input type="text" readonly="readonly" value="o">
    </li>
    <li>
        <div data-icon="p" class="icon"></div>
        <input type="text" readonly="readonly" value="p">
    </li>
    <li>
        <div data-icon="q" class="icon"></div>
        <input type="text" readonly="readonly" value="q">
    </li>
    <li>
        <div data-icon="m" class="icon"></div>
        <input type="text" readonly="readonly" value="m">
    </li>
    <li>
        <div data-icon="n" class="icon"></div>
        <input type="text" readonly="readonly" value="n">
    </li>
    <li>
        <div data-icon="i" class="icon"></div>
        <input type="text" readonly="readonly" value="i">
    </li>
    <li>
        <div data-icon="j" class="icon"></div>
        <input type="text" readonly="readonly" value="j">
    </li>
    <li>
        <div data-icon="k" class="icon"></div>
        <input type="text" readonly="readonly" value="k">
    </li>
    <li>
        <div data-icon="s" class="icon"></div>
        <input type="text" readonly="readonly" value="s">
    </li>
    <li>
        <div data-icon="t" class="icon"></div>
        <input type="text" readonly="readonly" value="t">
    </li>
    <li>
        <div data-icon="u" class="icon"></div>
        <input type="text" readonly="readonly" value="u">
    </li>
    <li>
        <div data-icon="v" class="icon"></div>
        <input type="text" readonly="readonly" value="v">
    </li>
    <li>
        <div data-icon="w" class="icon"></div>
        <input type="text" readonly="readonly" value="w">
    </li>
    <li>
        <div data-icon="x" class="icon"></div>
        <input type="text" readonly="readonly" value="x">
    </li>
    <li>
        <div data-icon="y" class="icon"></div>
        <input type="text" readonly="readonly" value="y">
    </li>
    <li>
        <div data-icon="z" class="icon"></div>
        <input type="text" readonly="readonly" value="z">
    </li>
    <li>
        <div data-icon="A" class="icon"></div>
        <input type="text" readonly="readonly" value="A">
    </li>
    <li>
        <div data-icon="B" class="icon"></div>
        <input type="text" readonly="readonly" value="B">
    </li>
    <li>
        <div data-icon="C" class="icon"></div>
        <input type="text" readonly="readonly" value="C">
    </li>
    <li>
        <div data-icon="D" class="icon"></div>
        <input type="text" readonly="readonly" value="D">
    </li>
    <li>
        <div data-icon="E" class="icon"></div>
        <input type="text" readonly="readonly" value="E">
    </li>
    <li>
        <div data-icon="F" class="icon"></div>
        <input type="text" readonly="readonly" value="F">
    </li>
    <li>
        <div data-icon="G" class="icon"></div>
        <input type="text" readonly="readonly" value="G">
    </li>
    <li>
        <div data-icon="H" class="icon"></div>
        <input type="text" readonly="readonly" value="H">
    </li>
    <li>
        <div data-icon="I" class="icon"></div>
        <input type="text" readonly="readonly" value="I">
    </li>
    <li>
        <div data-icon="J" class="icon"></div>
        <input type="text" readonly="readonly" value="J">
    </li>
    <li>
        <div data-icon="K" class="icon"></div>
        <input type="text" readonly="readonly" value="K">
    </li>
    <li>
        <div data-icon="L" class="icon"></div>
        <input type="text" readonly="readonly" value="L">
    </li>
    <li>
        <div data-icon="N" class="icon"></div>
        <input type="text" readonly="readonly" value="N">
    </li>
    <li>
        <div data-icon="O" class="icon"></div>
        <input type="text" readonly="readonly" value="O">
    </li>
    <li>
        <div data-icon="P" class="icon"></div>
        <input type="text" readonly="readonly" value="P">
    </li>
    <li>
        <div data-icon="Q" class="icon"></div>
        <input type="text" readonly="readonly" value="Q">
    </li>
    <li>
        <div data-icon="R" class="icon"></div>
        <input type="text" readonly="readonly" value="R">
    </li>
    <li>
        <div data-icon="M" class="icon"></div>
        <input type="text" readonly="readonly" value="M">
    </li>
    <li>
        <div data-icon="S" class="icon"></div>
        <input type="text" readonly="readonly" value="S">
    </li>
    <li>
        <div data-icon="T" class="icon"></div>
        <input type="text" readonly="readonly" value="T">
    </li>
    <li>
        <div data-icon="U" class="icon"></div>
        <input type="text" readonly="readonly" value="U">
    </li>
    <li>
        <div data-icon="W" class="icon"></div>
        <input type="text" readonly="readonly" value="W">
    </li>
    <li>
        <div data-icon="X" class="icon"></div>
        <input type="text" readonly="readonly" value="X">
    </li>
    <li>
        <div data-icon="V" class="icon"></div>
        <input type="text" readonly="readonly" value="V">
    </li>
    <li>
        <div data-icon="Y" class="icon"></div>
        <input type="text" readonly="readonly" value="Y">
    </li>
    <li>
        <div data-icon="Z" class="icon"></div>
        <input type="text" readonly="readonly" value="Z">
    </li>
    <li>
        <div data-icon="0" class="icon"></div>
        <input type="text" readonly="readonly" value="0">
    </li>
    <li>
        <div data-icon="1" class="icon"></div>
        <input type="text" readonly="readonly" value="1">
    </li>
    <li>
        <div data-icon="l" class="icon"></div>
        <input type="text" readonly="readonly" value="l">
    </li>
    <li>
        <div data-icon="2" class="icon"></div>
        <input type="text" readonly="readonly" value="2">
    </li>
    <li>
        <div data-icon="4" class="icon"></div>
        <input type="text" readonly="readonly" value="4">
    </li>
    <li>
        <div data-icon="5" class="icon"></div>
        <input type="text" readonly="readonly" value="5">
    </li>
    <li>
        <div data-icon="6" class="icon"></div>
        <input type="text" readonly="readonly" value="6">
    </li>
    <li>
        <div data-icon="7" class="icon"></div>
        <input type="text" readonly="readonly" value="7">
    </li>
    <li>
        <div data-icon="8" class="icon"></div>
        <input type="text" readonly="readonly" value="8">
    </li>
    <li>
        <div data-icon="9" class="icon"></div>
        <input type="text" readonly="readonly" value="9">
    </li>
    <li>
        <div data-icon="!" class="icon"></div>
        <input type="text" readonly="readonly" value="!">
    </li>
    <li>
        <div data-icon="&#34;" class="icon"></div>
        <input type="text" readonly="readonly" value="&quot;">
    </li>
    <li>
        <div data-icon="#" class="icon"></div>
        <input type="text" readonly="readonly" value="#">
    </li>
    <li>
        <div data-icon="$" class="icon"></div>
        <input type="text" readonly="readonly" value="$">
    </li>
    <li>
        <div data-icon="&" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;">
    </li>
    <li>
        <div data-icon="'" class="icon"></div>
        <input type="text" readonly="readonly" value="&#39;">
    </li>
    <li>
        <div data-icon="(" class="icon"></div>
        <input type="text" readonly="readonly" value="(">
    </li>
    <li>
        <div data-icon=")" class="icon"></div>
        <input type="text" readonly="readonly" value=")">
    </li>
    <li>
        <div data-icon="," class="icon"></div>
        <input type="text" readonly="readonly" value=",">
    </li>
    <li>
        <div data-icon="-" class="icon"></div>
        <input type="text" readonly="readonly" value="-">
    </li>
    <li>
        <div data-icon="." class="icon"></div>
        <input type="text" readonly="readonly" value=".">
    </li>
    <li>
        <div data-icon="/" class="icon"></div>
        <input type="text" readonly="readonly" value="/">
    </li>
    <li>
        <div data-icon=":" class="icon"></div>
        <input type="text" readonly="readonly" value=":">
    </li>
    <li>
        <div data-icon=";" class="icon"></div>
        <input type="text" readonly="readonly" value=";">
    </li>
    <li>
        <div data-icon="?" class="icon"></div>
        <input type="text" readonly="readonly" value="?">
    </li>
    <li>
        <div data-icon="@" class="icon"></div>
        <input type="text" readonly="readonly" value="@">
    </li>
    <li>
        <div data-icon="[" class="icon"></div>
        <input type="text" readonly="readonly" value="[">
    </li>
    <li>
        <div data-icon="]" class="icon"></div>
        <input type="text" readonly="readonly" value="]">
    </li>
    <li>
        <div data-icon="^" class="icon"></div>
        <input type="text" readonly="readonly" value="^">
    </li>
    <li>
        <div data-icon="_" class="icon"></div>
        <input type="text" readonly="readonly" value="_">
    </li>
    <li>
        <div data-icon="`" class="icon"></div>
        <input type="text" readonly="readonly" value="`">
    </li>
    <li>
        <div data-icon="{" class="icon"></div>
        <input type="text" readonly="readonly" value="{">
    </li>
    <li>
        <div data-icon="}" class="icon"></div>
        <input type="text" readonly="readonly" value="}">
    </li>
    <li>
        <div data-icon="~" class="icon"></div>
        <input type="text" readonly="readonly" value="~">
    </li>
    <li>
        <div data-icon="\" class="icon"></div>
        <input type="text" readonly="readonly" value="\">
    </li>
    <li>
        <div data-icon="&#xe001;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe001;">
    </li>
    <li>
        <div data-icon="&#xe002;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe002;">
    </li>
    <li>
        <div data-icon="&#xe003;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe003;">
    </li>
    <li>
        <div data-icon="&#xe004;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe004;">
    </li>
    <li>
        <div data-icon="&#xe005;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe005;">
    </li>
    <li>
        <div data-icon="&#xe006;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe006;">
    </li>
    <li>
        <div data-icon="&#xe007;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe007;">
    </li>
    <li>
        <div data-icon="&#xe008;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe008;">
    </li>
    <li>
        <div data-icon="&#xe009;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe009;">
    </li>
    <li>
        <div data-icon="&#xe00a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe00a;">
    </li>
    <li>
        <div data-icon="&#xe00b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe00b;">
    </li>
    <li>
        <div data-icon="&#xe00c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe00c;">
    </li>
    <li>
        <div data-icon="&#xe00d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe00d;">
    </li>
    <li>
        <div data-icon="&#xe00e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe00e;">
    </li>
    <li>
        <div data-icon="&#xe00f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe00f;">
    </li>
    <li>
        <div data-icon="&#xe011;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe011;">
    </li>
    <li>
        <div data-icon="&#xe012;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe012;">
    </li>
    <li>
        <div data-icon="&#xe013;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe013;">
    </li>
    <li>
        <div data-icon="%" class="icon"></div>
        <input type="text" readonly="readonly" value="%">
    </li>
    <li>
        <div data-icon="*" class="icon"></div>
        <input type="text" readonly="readonly" value="*">
    </li>
    <li>
        <div data-icon="|" class="icon"></div>
        <input type="text" readonly="readonly" value="|">
    </li>
    <li>
        <div data-icon="&#xe014;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe014;">
    </li>
    <li>
        <div data-icon="&#xe015;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe015;">
    </li>
    <li>
        <div data-icon="&#xe016;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe016;">
    </li>
    <li>
        <div data-icon="&#xe017;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe017;">
    </li>
    <li>
        <div data-icon="&#xe018;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe018;">
    </li>
    <li>
        <div data-icon="&#xe019;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe019;">
    </li>
    <li>
        <div data-icon="&#xe01a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe01a;">
    </li>
    <li>
        <div data-icon="&#xe01b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe01b;">
    </li>
    <li>
        <div data-icon="&#xe01c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe01c;">
    </li>
    <li>
        <div data-icon="&#xe01d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe01d;">
    </li>
    <li>
        <div data-icon="&#xe01e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe01e;">
    </li>
    <li>
        <div data-icon="&#xe01f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe01f;">
    </li>
    <li>
        <div data-icon="&#xe020;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe020;">
    </li>
    <li>
        <div data-icon="&#xe021;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe021;">
    </li>
    <li>
        <div data-icon="&#xe022;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe022;">
    </li>
    <li>
        <div data-icon="&#xe023;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe023;">
    </li>
    <li>
        <div data-icon="&#xe024;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe024;">
    </li>
    <li>
        <div data-icon="&#xe025;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe025;">
    </li>
    <li>
        <div data-icon="&#xe026;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe026;">
    </li>
    <li>
        <div data-icon="&#xe027;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe027;">
    </li>
    <li>
        <div data-icon="&#xe028;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe028;">
    </li>
    <li>
        <div data-icon="&#xe029;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe029;">
    </li>
    <li>
        <div data-icon="&#xe02a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe02a;">
    </li>
    <li>
        <div data-icon="&#xe02b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe02b;">
    </li>
    <li>
        <div data-icon="&#xe02c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe02c;">
    </li>
    <li>
        <div data-icon="&#xe02d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe02d;">
    </li>
    <li>
        <div data-icon="&#xe02e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe02e;">
    </li>
    <li>
        <div data-icon="&#xe02f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe02f;">
    </li>
    <li>
        <div data-icon="&#xe030;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe030;">
    </li>
    <li>
        <div data-icon="&#xe031;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe031;">
    </li>
    <li>
        <div data-icon="&#xe032;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe032;">
    </li>
    <li>
        <div data-icon="&#xe033;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe033;">
    </li>
    <li>
        <div data-icon="&#xe034;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe034;">
    </li>
    <li>
        <div data-icon="&#xe035;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe035;">
    </li>
    <li>
        <div data-icon="&#xe036;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe036;">
    </li>
    <li>
        <div data-icon="&#xe037;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe037;">
    </li>
    <li>
        <div data-icon="&#xe03a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe03a;">
    </li>
    <li>
        <div data-icon="&#xe03b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe03b;">
    </li>
    <li>
        <div data-icon="&#xe03c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe03c;">
    </li>
    <li>
        <div data-icon="&#xe03d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe03d;">
    </li>
    <li>
        <div data-icon="&#xe03e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe03e;">
    </li>
    <li>
        <div data-icon="&#xe03f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe03f;">
    </li>
    <li>
        <div data-icon="&#xe040;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe040;">
    </li>
    <li>
        <div data-icon="&#xe041;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe041;">
    </li>
    <li>
        <div data-icon="&#xe042;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe042;">
    </li>
    <li>
        <div data-icon="&#xe043;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe043;">
    </li>
    <li>
        <div data-icon="&#xe044;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe044;">
    </li>
    <li>
        <div data-icon="&#xe045;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe045;">
    </li>
    <li>
        <div data-icon="&#xe04a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe04a;">
    </li>
    <li>
        <div data-icon="&#xe04c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe04c;">
    </li>
    <li>
        <div data-icon="&#xe04d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe04d;">
    </li>
    <li>
        <div data-icon="&#xe04e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe04e;">
    </li>
    <li>
        <div data-icon="&#xe04f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe04f;">
    </li>
    <li>
        <div data-icon="&#xe053;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe053;">
    </li>
    <li>
        <div data-icon="&#xe054;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe054;">
    </li>
    <li>
        <div data-icon="&#xe055;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe055;">
    </li>
    <li>
        <div data-icon="&#xe056;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe056;">
    </li>
    <li>
        <div data-icon="&#xe057;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe057;">
    </li>
    <li>
        <div data-icon="&#xe058;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe058;">
    </li>
    <li>
        <div data-icon="&#xe059;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe059;">
    </li>
    <li>
        <div data-icon="&#xe05a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe05a;">
    </li>
    <li>
        <div data-icon="&#xe05b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe05b;">
    </li>
    <li>
        <div data-icon="&#xe05e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe05e;">
    </li>
    <li>
        <div data-icon="&#xe061;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe061;">
    </li>
    <li>
        <div data-icon="&#xe062;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe062;">
    </li>
    <li>
        <div data-icon="&#xe063;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe063;">
    </li>
    <li>
        <div data-icon="3" class="icon"></div>
        <input type="text" readonly="readonly" value="3">
    </li>
    <li>
        <div data-icon="+" class="icon"></div>
        <input type="text" readonly="readonly" value="+">
    </li>
    <li>
        <div data-icon="<" class="icon"></div>
        <input type="text" readonly="readonly" value="&lt;">
    </li>
    <li>
        <div data-icon="=" class="icon"></div>
        <input type="text" readonly="readonly" value="=">
    </li>
    <li>
        <div data-icon=">" class="icon"></div>
        <input type="text" readonly="readonly" value="&gt;">
    </li>
    <li>
        <div data-icon="&#xe000;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe000;">
    </li>
    <li>
        <div data-icon="&#xe010;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe010;">
    </li>
    <li>
        <div data-icon="&#xe038;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe038;">
    </li>
    <li>
        <div data-icon="&#xe039;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe039;">
    </li>
    <li>
        <div data-icon="&#xe046;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe046;">
    </li>
    <li>
        <div data-icon="&#xe047;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe047;">
    </li>
    <li>
        <div data-icon="&#xe048;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe048;">
    </li>
    <li>
        <div data-icon="&#xe04b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe04b;">
    </li>
    <li>
        <div data-icon="&#xe050;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe050;">
    </li>
    <li>
        <div data-icon="&#xe051;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe051;">
    </li>
    <li>
        <div data-icon="&#xe049;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe049;">
    </li>
    <li>
        <div data-icon="&#xe052;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe052;">
    </li>
    <li>
        <div data-icon="&#xe05c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe05c;">
    </li>
    <li>
        <div data-icon="&#xe05d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe05d;">
    </li>
    <li>
        <div data-icon="&#xe05f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe05f;">
    </li>
    <li>
        <div data-icon="&#xe060;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe060;">
    </li>
    <li>
        <div data-icon="&#xe064;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe064;">
    </li>
    <li>
        <div data-icon="&#xe065;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe065;">
    </li>
    <li>
        <div data-icon="&#xe066;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe066;">
    </li>
    <li>
        <div data-icon="&#xe067;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe067;">
    </li>
    <li>
        <div data-icon="&#xe068;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe068;">
    </li>
    <li>
        <div data-icon="&#xe069;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe069;">
    </li>
    <li>
        <div data-icon="&#xe06a;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe06a;">
    </li>
    <li>
        <div data-icon="r" class="icon"></div>
        <input type="text" readonly="readonly" value="r">
    </li>
    <li>
        <div data-icon="&#xe06b;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe06b;">
    </li>
    <li>
        <div data-icon="&#xe06c;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe06c;">
    </li>
    <li>
        <div data-icon="&#xe06d;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe06d;">
    </li>
    <li>
        <div data-icon="&#xe06e;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe06e;">
    </li>
    <li>
        <div data-icon="&#xe06f;" class="icon"></div>
        <input type="text" readonly="readonly" value="&amp;#xe06f;">
    </li>
</ul>

<script>
(function() {
    var glyphs, i, len, ref;

    ref = document.getElementsByClassName('glyphs');
    for (i = 0, len = ref.length; i < len; i++) {
        glyphs = ref[i];
        glyphs.addEventListener('click', function(event) {
            if (event.target.tagName === 'INPUT') {
                return event.target.select();
            }
        });
    }
}).call(this);
</script>
