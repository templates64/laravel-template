@extends('sunio::master')

@section('css')

    @include('sunio::compass.includes.styles')

@stop

@section('page_header')
    <h1 class="page-title">
        <i class="sunio-compass"></i>
        <p> {{ __('sunio::generic.compass') }}</p>
        <span class="page-description">{{ __('sunio::compass.welcome') }}</span>
    </h1>
@stop

@section('content')

    <div id="gradient_bg"></div>

    <div class="container-fluid">
        @include('sunio::alerts')
    </div>

    <div class="page-content compass container-fluid">
        <ul class="nav nav-tabs">
          <li @if(empty($active_tab) || (isset($active_tab) && $active_tab == 'resources')){!! 'class="active"' !!}@endif><a data-toggle="tab" href="#resources"><i class="sunio-book"></i> {{ __('sunio::compass.resources.title') }}</a></li>
          <li @if($active_tab == 'commands'){!! 'class="active"' !!}@endif><a data-toggle="tab" href="#commands"><i class="sunio-terminal"></i> {{ __('sunio::compass.commands.title') }}</a></li>
          <li @if($active_tab == 'logs'){!! 'class="active"' !!}@endif><a data-toggle="tab" href="#logs"><i class="sunio-logbook"></i> {{ __('sunio::compass.logs.title') }}</a></li>
        </ul>

        <div class="tab-content">
            <div id="resources" class="tab-pane fade in @if(empty($active_tab) || (isset($active_tab) && $active_tab == 'resources')){!! 'active' !!}@endif">
                <h3><i class="sunio-book"></i> {{ __('sunio::compass.resources.title') }} <small>{{ __('sunio::compass.resources.text') }}</small></h3>

                <div class="collapsible">
                    <div class="collapse-head" data-toggle="collapse" data-target="#links" aria-expanded="true" aria-controls="links">
                        <h4>{{ __('sunio::compass.links.title') }}</h4>
                        <i class="sunio-angle-down"></i>
                        <i class="sunio-angle-up"></i>
                    </div>
                    <div class="collapse-content collapse in" id="links">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="https://docs.laravelsunio.com" target="_blank" class="sunio-link" style="background-image:url('{{ sunio_asset('images/compass/documentation.jpg') }}')">
                                    <span class="resource_label"><i class="sunio-documentation"></i> <span class="copy">{{ __('sunio::compass.links.documentation') }}</span></span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="https://laravelsunio.com" target="_blank" class="sunio-link" style="background-image:url('{{ sunio_asset('images/compass/sunio-home.jpg') }}')">
                                    <span class="resource_label"><i class="sunio-browser"></i> <span class="copy">{{ __('sunio::compass.links.sunio_homepage') }}</span></span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="https://larapack.io" target="_blank" class="sunio-link" style="background-image:url('{{ sunio_asset('images/compass/hooks.jpg') }}')">
                                    <span class="resource_label"><i class="sunio-hook"></i> <span class="copy">{{ __('sunio::compass.links.sunio_hooks') }}</span></span>
                                </a>
                            </div>
                        </div>
                    </div>
              </div>

              <div class="collapsible">

                <div class="collapse-head" data-toggle="collapse" data-target="#fonts" aria-expanded="true" aria-controls="fonts">
                    <h4>{{ __('sunio::compass.fonts.title') }}</h4>
                    <i class="sunio-angle-down"></i>
                    <i class="sunio-angle-up"></i>
                </div>

                <div class="collapse-content collapse in" id="fonts">

                    @include('sunio::compass.includes.fonts')

                </div>

              </div>
            </div>

          <div id="commands" class="tab-pane fade in @if($active_tab == 'commands'){!! 'active' !!}@endif">
            <h3><i class="sunio-terminal"></i> {{ __('sunio::compass.commands.title') }} <small>{{ __('sunio::compass.commands.text') }}</small></h3>
            <div id="command_lists">
                @include('sunio::compass.includes.commands')
            </div>

          </div>
          <div id="logs" class="tab-pane fade in @if($active_tab == 'logs'){!! 'active' !!}@endif">
            <div class="row">

                @include('sunio::compass.includes.logs')

            </div>
          </div>
        </div>

    </div>

@stop
@section('javascript')
    <script>
        $('document').ready(function(){
            $('.collapse-head').click(function(){
                var collapseContainer = $(this).parent();
                if(collapseContainer.find('.collapse-content').hasClass('in')){
                    collapseContainer.find('.sunio-angle-up').fadeOut('fast');
                    collapseContainer.find('.sunio-angle-down').fadeIn('slow');
                } else {
                    collapseContainer.find('.sunio-angle-down').fadeOut('fast');
                    collapseContainer.find('.sunio-angle-up').fadeIn('slow');
                }
            });
        });
    </script>
    <!-- JS for commands -->
    <script>

        $(document).ready(function(){
            $('.command').click(function(){
                $(this).find('.cmd_form').slideDown();
                $(this).addClass('more_args');
                $(this).find('input[type="text"]').focus();
            });

            $('.close-output').click(function(){
                $('#commands pre').slideUp();
            });
        });

    </script>

    <!-- JS for logs -->
    <script>
      $(document).ready(function () {
        $('.table-container tr').on('click', function () {
          $('#' + $(this).data('display')).toggle();
        });
        $('#table-log').DataTable({
          "order": [1, 'desc'],
          "stateSave": true,
          "language": {!! json_encode(__('sunio::datatable')) !!},
          "stateSaveCallback": function (settings, data) {
            window.localStorage.setItem("datatable", JSON.stringify(data));
          },
          "stateLoadCallback": function (settings) {
            var data = JSON.parse(window.localStorage.getItem("datatable"));
            if (data) data.start = 0;
            return data;
          }
        });

        $('#delete-log, #delete-all-log').click(function () {
          return confirm('{{ __('sunio::generic.are_you_sure') }}');
        });
      });
    </script>
@stop
