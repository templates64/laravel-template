@extends('sunio::master')

@section('page_title', __('sunio::generic.media'))

@section('content')
    <div class="page-content container-fluid">
        @include('sunio::alerts')
        <div class="row">
            <div class="col-md-12">

                <div class="admin-section-title">
                    <h3><i class="sunio-images"></i> {{ __('sunio::generic.media') }}</h3>
                </div>
                <div class="clear"></div>
                <div id="filemanager">
                    <media-manager
                        base-path="{{ config('sunio.media.path', '/') }}"
                        :show-folders="{{ config('sunio.media.show_folders', true) ? 'true' : 'false' }}"
                        :allow-upload="{{ config('sunio.media.allow_upload', true) ? 'true' : 'false' }}"
                        :allow-move="{{ config('sunio.media.allow_move', true) ? 'true' : 'false' }}"
                        :allow-delete="{{ config('sunio.media.allow_delete', true) ? 'true' : 'false' }}"
                        :allow-create-folder="{{ config('sunio.media.allow_create_folder', true) ? 'true' : 'false' }}"
                        :allow-rename="{{ config('sunio.media.allow_rename', true) ? 'true' : 'false' }}"
                        :allow-crop="{{ config('sunio.media.allow_crop', true) ? 'true' : 'false' }}"
                        :details="{{ json_encode(['thumbnails' => config('sunio.media.thumbnails', []), 'watermark' => config('sunio.media.watermark', (object)[])]) }}"
                        ></media-manager>
                </div>
            </div><!-- .row -->
        </div><!-- .col-md-12 -->
    </div><!-- .page-content container-fluid -->
@stop

@section('javascript')
<script>
new Vue({
    el: '#filemanager'
});
</script>
@endsection
