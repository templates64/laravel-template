<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');


Route::group(['prefix' => 'admin'], function () {
    Sunio::routes();
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/login', 'AuthController@login')->name('user.login');
});


Route::group(['prefix' => 'posts'], function () {
    Route::get('/', 'PostController@index')->name('posts.index');
    Route::get('/{id}/{slug}', 'PostController@detail')->name('posts.detail');
});

Route::group(['prefix' => 'products'], function () {
    Route::resource('/', ProductController::class, [
        'names' => [
            'index' => 'products.index'
        ]
    ]);
    Route::get('/cart', 'ProductController@cart')->name('products.cart');
    Route::get('/checkout', 'ProductController@checkout')->name('products.checkout');
    Route::get('/{id}/{slug}', 'ProductController@detail')->name('products.detail');
});

Route::resource('carts', CartController::class);

Route::get('/contact', 'ContactController@index')->name('contact');
