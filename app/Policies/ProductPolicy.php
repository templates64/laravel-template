<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use SIO\Sunio\Contracts\User;
use SIO\Sunio\Policies\BasePolicy;
class ProductPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
    * Determine if the given model can be viewed by the user.
    *
    * @param \SIO\Sunio\Contracts\User $user
    * @param  $model
    *
    * @return bool
    */
   public function read(User $user, $model)
   {
       // Does this product belong to the current user?
       $current = $user->id === $model->author_id;

       return $current || $this->checkPermission($user, $model, 'read');
   }

   /**
    * Determine if the given model can be edited by the user.
    *
    * @param \SIO\Sunio\Contracts\User $user
    * @param  $model
    *
    * @return bool
    */
   public function edit(User $user, $model)
   {
       // Does this product belong to the current user?
       $current = $user->id === $model->author_id;

       return $current || $this->checkPermission($user, $model, 'edit');
   }

   /**
    * Determine if the given model can be deleted by the user.
    *
    * @param \SIO\Sunio\Contracts\User $user
    * @param  $model
    *
    * @return bool
    */
   public function delete(User $user, $model)
   {
       // Does this product belong to the current user?
       $current = $user->id === $model->author_id;

       // Has this already been deleted?
       $soft_delete = $model->deleted_at && in_array(\Illuminate\Database\Eloquent\SoftDeletes::class, class_uses_recursive($model));

       return !$soft_delete && ($current || $this->checkPermission($user, $model, 'delete'));
   }
}
