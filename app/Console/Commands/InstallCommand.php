<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sio:install {--R|reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if($this->option('reset')) {
            $this->warn('> php artisan migrate:reset');
            $this->call('migrate:reset');
        }

        $this->warn('> php artisan sunio:install --with-dummy');
        $this->call('sunio:install', ['--with-dummy' => null]);

        $this->warn('> php artisan db:seed');
        $this->call('db:seed');
    }
}
