<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use SIO\Sunio\Http\Controllers\Controller as AtvController;
use Illuminate\Support\Facades\Auth;
use SIO\Sunio\Facades\Sunio;

class AuthController extends AtvController
{
    use AuthenticatesUsers;

    public function login()
    {
        if ($this->guard()->user()) {
            return redirect()->route('home');
        }

        return view('pages.login');
    }

}
