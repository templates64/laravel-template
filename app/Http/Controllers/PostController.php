<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('pages.posts.index');
    }
    
    public function detail($id, $slug)
    {
        return view('pages.posts.detail');
    }

}
