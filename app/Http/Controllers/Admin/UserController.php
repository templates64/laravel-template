<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SIO\Sunio\Http\Controllers\SunioUserController;
use App\Models\User;
use SIO\Sunio\Facades\Sunio;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\UserMeta;
use SIO\Sunio\Events\BreadDataAdded;
use Carbon\Carbon;

class UserController extends SunioUserController
{
    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************

    public function edit(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $meta_fields = $user->userMetas;


        $slug = $this->getSlug($request);

        $dataType = Sunio::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model->query();

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $query = $query->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $query->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$query, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $view = 'sunio::bread.edit-add';

        if (view()->exists("sunio::$slug.edit-add")) {
            $view = "sunio::$slug.edit-add";
        }

        return Sunio::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'meta_fields'));
    }
    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $req = $request->all();
        $user = User::findOrFail($id);
        $meta_fields = $user->userMetas;
        foreach ($meta_fields as $meta) {
            if (array_key_exists($meta->meta_key, $req)) {
                $meta->meta_value = $req[$meta->meta_key];
                $meta->save();
            }
        }
        return parent::update($request, $id);
    }

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Sunio::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        
        UserMeta::insert([
            ['user_id' => $data->id, 'meta_key' => 'phone', 'meta_value' => $request->phone, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['user_id' => $data->id, 'meta_key' => 'address', 'meta_value' => $request->address, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("sunio.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('sunio::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }
}
