<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SIO\Sunio\Http\Controllers\SunioUserController;
use SIO\Sunio\Facades\Sunio;
use SIO\Sunio\Events\BreadDataAdded;
use App\Models\User;
use App\Models\UserMeta;

class UserMetaController extends SunioUserController
{
    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Sunio::model('DataType')->where('slug', '=', $slug)->first();
        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();

        if ($request->add_to_all_user == 'on') {
            $users = User::all();
            foreach ($users as $user) {
                $meta = $this->findUserMeta($user->id, 'phonse');
                if (!$meta->exists) {
                    $meta->fill([
                        'user_id' => $user->id,
                        'meta_key' => $request->meta_key,
                        'meta_value' => $request->meta_value,
                    ])->save();
                }
            }

            $redirect = redirect()->route("sunio.{$dataType->slug}.index");
            return $redirect->with([
                'message'    => __('sunio::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            
            event(new BreadDataAdded($dataType, $data));
        }




        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("sunio.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('sunio::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    protected function findUserMeta($user_id, $meta_key)
    {
        // dd($user_id);
        return UserMeta::firstOrNew(['user_id' => $user_id, 'meta_key' => $meta_key]);
    }
}
