<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function posts()
    {
        return $this->belongsToMany(Post::class, PostCategory::class);
    }

    public function scopePost($query)
    {
        return $query->where('category_type', 'post');
    }
    
    public function scopeProduct($query)
    {
        return $query->where('category_type', 'product');
    }

}
